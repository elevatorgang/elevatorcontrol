package ElevatorSimulator;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.concurrent.LinkedBlockingQueue;

/*****************************************************************************
 * Class Functions
 * - Constructors
 * 	- Takes port, main process id and datagram new message object as inputs
 * - run
 * 	- Calls function to listen on port
 * - Listen
 * 	- Listens on port
 * 	- Calls Process Datagram function
 * - ProcessDatagram
 * 	- Processes the received datagram
 * 	- Sends message to main process
 * - println
 * 	- Prints to standard output from asynchronous context
 ****************************************************************************/
public class DataSocketListenerSched implements Runnable{
	// Declare class attributes
	private int Port;
	private DatagramSocket sendReceiveSocket;
	private DatagramMessages DMessages;
	private int sendPort;
	private OutMessage outMsg;
	private int Type;
	private LinkedBlockingQueue<byte[]> inMsg;
	private String IP;
	private DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss.SSS");
	private LocalDateTime now;
	/*****************************************************************************
	 * Constructor
	 * 	- Takes in:
	 * 		- Port: To listen for DatagramPackets on
	 * 		- Parent Process ID: To be able to notify main process of new messages
	 ****************************************************************************/
	public DataSocketListenerSched(int newPort, DatagramMessages newDMessages, OutMessage newOutMsg, int newType, int newSendPort, LinkedBlockingQueue<byte[]> newInMsg, String newIP) {
		// Initialize class attributes
		Port = newPort;
		sendPort = newSendPort;
		DMessages = newDMessages;
		outMsg = newOutMsg;
		Type = newType;
		inMsg = newInMsg;
		IP = newIP;
		try {
			sendReceiveSocket = new DatagramSocket(Port);
		} catch (SocketException e) {
			e.printStackTrace();
		}
	}
	
	/*****************************************************************************
	 * Run
	 * 	- Starts DataSocket Listener thread:
	 ****************************************************************************/
	public void run() {
		// Print start message
		println("DataSocketListener: DatagramSocket Listener has started running. Listening on port: " + Integer.toString(Port));
		
		while(true) {
			// Listen on port
			Listen();
		}
	}
	
	/*****************************************************************************
	 * checkOutMsg
	 * 	- Checks if there is an out message to run
	 ****************************************************************************/
	private void checkOutMsg() {
		synchronized(outMsg) {
			// Check if there is a message
			if (outMsg.length != -1 && outMsg.type == Type) {
				// Send the message
				ProcessSendPacket(outMsg.msg, outMsg.length);
				
				// reset outMsg object
				outMsg.length = -1;
				outMsg.type = -1;
			}
		}
	}
	
	/*****************************************************************************
	 * Listens on port for datagram packets
	 * 	- Listens for packet
	 * 	- Sends packet for processing
	 ****************************************************************************/
	private void Listen() {
		// Declare receive packet
		byte[] data = new byte[6];
		DatagramPacket receivePacket = new DatagramPacket(data, data.length);
		
		// Listen on port
		try {
			// Set timeout
			sendReceiveSocket.setSoTimeout(500);
			// Listen on port
			sendReceiveSocket.receive(receivePacket);
		} catch (IOException e) {
//			println("DataSocketListener: Timed Out");
		}
		
		// Check for out messages
		checkOutMsg();
		
		// Process DatagramPacket
		ProcessDatagram(receivePacket, data);
	}
	
	/*****************************************************************************
	 * Processes datagram packet received and prints out the contents
	 ****************************************************************************/
	private void ProcessDatagram(DatagramPacket receivePacket, byte[] data) {
		// Declare local variables
		int len = receivePacket.getLength();
		
		// Process the received datagram
		data = receivePacket.getData();
		if (receivePacket.getAddress() != null) {
			//**/println("DataSocketListener: Received packet containing: ");
//			println("From host: " + receivePacket.getAddress());
//			println("Host port: " + receivePacket.getPort());
//			println("Length: " + len);
//			println("Containing: ");
			
			// Store message in byte array
			data = receivePacket.getData();
			inMsg.add(data);
			// Print byte message
			//**/printBMessage(data, data.length);
			
			// Notify Main Process Thread
//			synchronized (DMessages) {
//				
//				
//				// Push the message onto the message stack
//				DMessages.pushMessage(data);
//				
//				// Notify Main Process
//				DMessages.notifyAll();
//			}
		}
	}
	
	public void ProcessSendPacket(byte[] sendByteSeq, int length) {
		// Declare send packet
		byte[] data = new byte[length];
		DatagramPacket sendPacket = new DatagramPacket(data, data.length);
		
		// Format send packet
		try {
			// Create the packet to send
//			sendPacket = new DatagramPacket(sendByteSeq, sendByteSeq.length, 
//					InetAddress.getLocalHost(), sendPort);
			sendPacket = new DatagramPacket(sendByteSeq, sendByteSeq.length, 
					InetAddress.getByName(IP), sendPort);
		} catch (UnknownHostException e) {
			e.printStackTrace();
			System.exit(1);
		}
		
		// Print sending packet notification
		//**/println("DataSocketListener: Sending packet to port " + Integer.toString(sendPort) + ", with contents: ");
		//**/printBMessage(sendByteSeq, sendByteSeq.length);
		
		// Return send datagram packet
		SendThePacket(sendReceiveSocket, sendPacket);
	}
	
	public boolean SendThePacket(DatagramSocket sendReceiveSocket, DatagramPacket Packet) {
		try {
			// Send the packet
			sendReceiveSocket.send(Packet);
		} catch (IOException e) {
			e.printStackTrace();
			System.exit(1);
		}
		// Return success
		return true;
	}
	
	/*****************************************************************************
	 * Print byte array message
	 ****************************************************************************/
	private void printBMessage(byte[] msg, int length) {
		// Print each byte individually
		for (int i = 0; i < length; i++) {
			print(msg[i]);
		}
		// Print newline character
		print("\n");
	}
	
	/*****************************************************************************
	 * Used by threads to print to standard output
	 * 	- Remove IlleagalMonitorState error
	 ****************************************************************************/
	public void println(String x) {
		// Synchronized version of println for string
	    synchronized (this) {
	        System.out.print(x + "\n");
	    }
	}
	public void print(String x) {
		// Synchronized version of print for string
		synchronized (this) {
	        System.out.print(x);
	    }
	}
	public void print(Byte x) {
		// Synchronized version of print for byte
	    synchronized (this) {
	        System.out.print(x);
	    }
	}
}

