package ElevatorSimulator;
//Simulates the status of the door of the elevator
//Created by Matthew Fundarek 100941844
public class ElevatorDoor {
	private boolean isOpen = false;
	
	public ElevatorDoor() {
		
	}
	
	public boolean checkDoor() {
		return isOpen;
	}
	
	public void toggleDoor() {
		if(isOpen) {
			isOpen = false;
		}else if(!isOpen) {
			isOpen = true;
		}
	}
}
