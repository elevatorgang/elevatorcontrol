package ElevatorSimulator;
//Simulates the status of the elevator motor
//Created by Matthew Fundarek 100941844
public class ElevatorMotor {
	private boolean isOn = false;
	
	public ElevatorMotor() {
		
	}
	
	public boolean checkMotor() {
		return isOn;
	}
	
	public void toggleMotor() {
		if(isOn) {
			isOn = false;
		}else if(!isOn) {
			isOn = true;
		}
	}
}
