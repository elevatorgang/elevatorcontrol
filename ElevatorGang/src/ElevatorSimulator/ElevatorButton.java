package ElevatorSimulator;
//Created by Matthew Fundarek 100941844
import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.nio.charset.StandardCharsets;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;

public class ElevatorButton {
	
	private DatagramPacket sendPacket;
	private DatagramSocket sendSocket;
	private String SchedIP;
	int number;
	boolean isConfirmed = false;
	private int LoggerPort;
	private DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss.SSS");
	private LocalDateTime now;
	private long offset;
	private String LoggerIP;
	
	public ElevatorButton(int floor, String SIP, String LIP, int newLP, long newO) {
		number = floor;
		offset = newO;
		SchedIP = SIP;
		LoggerPort = newLP;
		LoggerIP = LIP;
		try {
	         
	         sendSocket = new DatagramSocket();
	         
	      } catch (SocketException se) {   // Can't create the socket.
	    	  se.printStackTrace();
		      System.exit(1);
	      }
	}
	
	public Instant pressButton(int elevatorNumber) {
		
		byte msg[] = new byte[6];
		msg[0] = (byte)2;
		msg[1] = (byte) elevatorNumber;
		msg[2] = (byte)((number - number%10)/10);
		msg[3] = (byte)(number%10);
		msg[4] = (byte)0;
		msg[5] = (byte)0;
//		if(number<=16) {
//			msg[4] = 0;
//			msg[5] = (byte) number;
//		}else {
//			int four = number / 16;
//			int five = number % 16;
//			msg[4] = (byte) four;
//			msg[5] = (byte) five;
//		}
				
		
	    try {
			//sendPacket = new DatagramPacket(msg, msg.length, InetAddress.getByName("172.17.40.149"), 1025);
			sendPacket = new DatagramPacket(msg, msg.length, InetAddress.getByName(SchedIP), 1025);
		} catch (UnknownHostException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
	   
	   
	    Instant sendTime = Instant.now();
		try {
	         sendSocket.send(sendPacket);
	      } catch (IOException e) {
	         e.printStackTrace();
	         System.exit(1);
	      }
		now = LocalDateTime.now();
		now.minus(offset, ChronoUnit.MILLIS);
		byte[] log = (dtf.format(now) + ":Elevator:: Elevator Button:: Sending button press for floor " + number + " on floor " + elevatorNumber).getBytes(StandardCharsets.UTF_8);
		try {
			//sendPacket = new DatagramPacket(log, msg.length, InetAddress.getByName("172.17.40.149"), 1025);
			sendPacket = new DatagramPacket(log, log.length, InetAddress.getByName(LoggerIP), LoggerPort);
		} catch (UnknownHostException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		try {
	         sendSocket.send(sendPacket);
	      } catch (IOException e) {
	         e.printStackTrace();
	         System.exit(1);
	      }
		return sendTime;
	}
}
