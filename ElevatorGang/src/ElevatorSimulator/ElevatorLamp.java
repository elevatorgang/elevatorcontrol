package ElevatorSimulator;
//Simulates the status of the floor lamps of an elevator
//Created by Matthew Fundarek 100941844
public class ElevatorLamp {
	private boolean isOn = false;
	
	public ElevatorLamp() {
		
	}
	
	public boolean checkLamp() {
		return isOn;
	}
	
	public void toggleLamp() {
		if(isOn) {
			isOn = false;
		}else if(!isOn) {
			isOn = true;
		}
	}
}
