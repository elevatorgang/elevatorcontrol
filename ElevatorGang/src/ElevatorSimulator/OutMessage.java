package ElevatorSimulator;

public class OutMessage {
	// Declare class attributes
	public byte[] msg;
	public int length;
	public int type;
	
	// Declare constructor
	public OutMessage(){
		// Initialize class attributes
		length = -1;
		msg = new byte[5];
		type = -1;
	}
}
