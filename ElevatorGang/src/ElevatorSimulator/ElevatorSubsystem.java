package ElevatorSimulator;
//Created by Matthew Fundarek 100941844, Majority of thread work done by Kurt




import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.nio.charset.StandardCharsets;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.Arrays;

import java.util.concurrent.LinkedBlockingQueue;

import MasterSimulator.Overseer;
import TimeSync.TimeSync;




public class ElevatorSubsystem implements Runnable{
	

	
	private DatagramMessages DMessagesSched;
	private DatagramMessages DMessagesFlr;
	private OutMessage outMsg;
	private int floors, elevs;
	private Elevator[] elevators;
	private DataSocketListener DSListenerScheduler;
	private DataSocketListener DSListenerFloors;
	private LinkedBlockingQueue<byte[]> inMsg;
	private LinkedBlockingQueue<Integer> newDest = new LinkedBlockingQueue<Integer>();
	private String FloorIP;
	private String SchedIP;
	private String LoggerIP;
	private Overseer o;
	private DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss.SSS");
	private LocalDateTime now;
	private long offset;
	private int LoggerPort;
	
	private void syncClock() {
		// NTP Clock Synchronization Test
		offset = TimeSync.getTimeOffset();
	}
	public ElevatorSubsystem(Overseer over, String FIP, String SIP, String LIP, int newLP) {
		LoggerPort = newLP;
		LoggerIP = LIP;
		outMsg = new OutMessage();
		floors = -1;
		elevs = -1;
		inMsg = new LinkedBlockingQueue<byte[]>();
		o = over;
		FloorIP = FIP;
		SchedIP = SIP;
		syncClock();
	}
	
	public void pressButton(int Elevator, int Floor) {
		//Presses the button for a given floor on a given elevator
		elevators[Elevator-1].pressButton(Floor);
	}
	
	public void newDestination(int Elevator, int floor) {
		//Changes the destination of a given elevator
		elevators[Elevator-1].newDestination(floor);
	}
	
	public void Confirm(int Elevator) {
		//Passes received confirmation to the elevator
		elevators[Elevator-1].isConfirmed();
	}
	
	
	private void checkMessages() {
		byte[] msg;
		try {
			msg = inMsg.take();
		} catch (InterruptedException e) {
			e.printStackTrace();
			return;
		}
		
		if (msg[0] > 6) {
			checkMessagesFloor(msg);
		}else {
			checkMessagesSched(msg);
		}
		
		// Check if there is a message from the scheduler
		//checkMessagesSched();
		
		// Check if there is a message from the elevators
		//checkMessagesFloor();
	}
	
	private void checkMessagesFloor(byte[] msg) {
		// Main Thread checks if message stack is empty and if it is empty main thread waits, 
		// if it is not empty main thread begins taking messages out
		/*synchronized (DMessagesFlr) {
			// Retrieve messages from the stack
			while(!DMessagesFlr.isEmpty()) {
				// Push new elevator message into elevator message stack
				messagesFlr.push(DMessagesFlr.popMessage());	
			}
		}
		// Delegate message
		while(!messagesFlr.isEmpty()) {
			*/// Retrieve message from message stack
			//byte[] msg = messagesFlr.pop();
			
			/**/
//			now = LocalDateTime.now();
//			System.out.print(dtf.format(now) +": Received message from floor: ");
//			System.out.print(msg[0]);
//			System.out.print(msg[1]);
//			System.out.print(msg[2]);
//			System.out.print(msg[3]);
//			System.out.print(msg[4]);
//			System.out.println(msg[5]);/**/
//			
			// Determine if message is button press or confirmation
			if (msg[0] == (byte)7) {
				// Store floor number
				int FloorNum = msg[3] * 10 + msg[4];
				int elnum = msg[5];
				// Send confirmation
				sendConfirmation(DSListenerFloors, msg);
				pressButton(elnum, FloorNum);
				now = LocalDateTime.now();
				now.minus(offset, ChronoUnit.MILLIS);
				String Log = dtf.format(now) + ":Elevator:: Button press message received";
				sendLog(Log);
				System.out.println(Log);
				
			}else if (msg[0] == (byte)8) {	
				int elnum = msg[3]; /**/ //4 to 2
				Confirm(elnum);
				now = LocalDateTime.now();
				now.minus(offset, ChronoUnit.MILLIS);
				String Log = dtf.format(now) +":Elevator:: Confirmation message received";
				sendLog(Log);
				System.out.println(Log);
			}else if (msg[0] == (byte)9) {
				int elnum = msg[3];
				if(msg[1]==0) {
					elevators[elnum-1].hardError=true;
				}else if(msg[1]==1 ) {
					elevators[elnum-1].transientError=true;
				}
				
			}
		//}
	}
	
	private void sendLog(String Log) {
		now = LocalDateTime.now();
		now.minus(offset, ChronoUnit.MILLIS);
		byte[] log = Log.getBytes(StandardCharsets.UTF_8);
		DatagramSocket sendSocket;
		DatagramPacket sendPacket;
		try {
			
			//sendPacket = new DatagramPacket(msg, msg.length, InetAddress.getByName("172.17.40.149"), 1025);
			sendPacket = new DatagramPacket(log, log.length, InetAddress.getByName(LoggerIP), LoggerPort);
			sendSocket = new DatagramSocket(1043);
	         sendSocket.send(sendPacket);
	         sendSocket.close();
		} catch (UnknownHostException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	private void checkMessagesSched(byte[] msg) {
		// Main Thread checks if message stack is empty and if it is empty main thread waits, 
		// if it is not empty main thread begins taking messages out
		/*synchronized (DMessagesSched) {
			// Retrieve messages from the stack
			while(!DMessagesSched.isEmpty()) {
				// Push new elevator message into elevator message stack
				messagesSched.push(DMessagesSched.popMessage());	
			}
		}
		// Delegate message
		while(!messagesSched.isEmpty()) {
			// Retrieve message from message stack
			byte[] msg = messagesSched.pop();*/
			
			/**/
//			now = LocalDateTime.now();
//			System.out.print(dtf.format(now) + ": Received message from Scheduler: ");
//			System.out.print(msg[0]);
//			System.out.print(msg[1]);
//			System.out.print(msg[2]);
//			System.out.print(msg[3]);
//			System.out.print(msg[4]);
//			System.out.println(msg[5]);/**/
			
			// Determine if message is new initialization, checking in, new destination or confirmation
			if (elevs == -1) {
				// Store number of floors and elevators
				floors = msg[2] * 10 + msg[3];
				elevs = msg[1]; // msg[0] * 10 +
				now = LocalDateTime.now();
				now.minus(offset, ChronoUnit.MILLIS);
				String Log = dtf.format(now) +":Elevator:: Initialization message received";
				sendLog(Log);
				System.out.println(Log);
				o.Init(elevs,  floors);
				
				// send confirmation message
				sendInit(DSListenerScheduler);
			}else if (msg[1] == (byte)0 && msg[2] == (byte)0 && msg[3] == (byte)0 && msg[4] == (byte)0 ) {
				
				sendConfirmation(DSListenerScheduler);
				now = LocalDateTime.now();
				now.minus(offset, ChronoUnit.MILLIS);
				String Log = dtf.format(now) + ":Elevator:: Check in received ";
				sendLog(Log);
				System.out.println(Log);
				
			}/**/else if (msg[0] == (byte)2) {/**/ // Changed from msg[4] != byte 0 and moved if up 1 level
				// Pass confirmation
				int elnum = msg[1];
				Confirm(elnum);
				now = LocalDateTime.now();
				now.minus(offset, ChronoUnit.MILLIS);
				String Log = dtf.format(now) + ":Elevator:: Confirmation received";
				sendLog(Log);
				System.out.println(Log);
			}/**/else if (msg[0] == (byte)1) {
				// Update for new destination
				int elnum = msg[1];
				int floornum = msg[2] * 10 + msg[3];
				newDestination(elnum, floornum);
				sendConfirmation(DSListenerScheduler);
				now = LocalDateTime.now();
				now.minus(offset, ChronoUnit.MILLIS);
				String Log = dtf.format(now) + ":Elevator:: New destination received for Elevator "+elnum+" to go to floor " +floornum;
				sendLog(Log);
				System.out.println(Log);
			}
		//}
	}
	
	private void sendConfirmation(DataSocketListener DSListener, byte[] input) {
		// Prepare confirmation message
		byte[] confirmationMsg = input;
		confirmationMsg[0] = 1;
		
		/**/
		now = LocalDateTime.now();
		now.minus(offset, ChronoUnit.MILLIS);
		String Log = dtf.format(now) + ":Elevator:: Sending Confirmation Message: ";
		System.out.print(Log);
		System.out.print(confirmationMsg[0]);
		System.out.print(confirmationMsg[1]);
		System.out.print(confirmationMsg[2]);
		System.out.print(confirmationMsg[3]);
		System.out.print(confirmationMsg[4]);
		System.out.println(confirmationMsg[5]);/**/
		sendLog(Log);
		System.out.println(Log);
		// Send confirmation
		synchronized(outMsg) {
			// Fill out message
			outMsg.length = 6;
			outMsg.msg = confirmationMsg;
			outMsg.type = 1;
			
			// Notify all 
			outMsg.notifyAll();
		}
	}
	
	private void sendInit(DataSocketListener DSListener) {
		// Prepare confirmation message
		byte[] confirmationMsg = new byte[6];
		Arrays.fill(confirmationMsg, (byte)9);
		
		/**/
		now = LocalDateTime.now();
		now.minus(offset, ChronoUnit.MILLIS);
		String Log = dtf.format(now) + ":Elevator:: Sending init confirmation message: ";
		System.out.print(Log);
		System.out.print(confirmationMsg[0]);
		System.out.print(confirmationMsg[1]);
		System.out.print(confirmationMsg[2]);
		System.out.print(confirmationMsg[3]);
		System.out.print(confirmationMsg[4]);
		System.out.println(confirmationMsg[5]);/**/
		sendLog(Log);
		System.out.println(Log);
		// Send confirmation
		synchronized(outMsg) {
			// Fill out message
			outMsg.length = 6;
			outMsg.msg = confirmationMsg;
			outMsg.type = 0;
			
			// Notify all 
			outMsg.notifyAll();
		}
	}
	private void sendConfirmation(DataSocketListener DSListener) {
		// Prepare confirmation message
		byte[] confirmationMsg = new byte[6];
		Arrays.fill(confirmationMsg, (byte)0);
		
		/**/
		now = LocalDateTime.now();
		now.minus(offset, ChronoUnit.MILLIS);
		String Log = dtf.format(now) + ":Elevator:: Sending confirmation message ";
		System.out.print(Log);
		System.out.print(confirmationMsg[0]);
		System.out.print(confirmationMsg[1]);
		System.out.print(confirmationMsg[2]);
		System.out.print(confirmationMsg[3]);
		System.out.print(confirmationMsg[4]);
		System.out.println(confirmationMsg[5]);/**/
		sendLog(Log);
		System.out.println(Log);
		// Send confirmation
		synchronized(outMsg) {
			// Fill out message
			outMsg.length = 6;
			outMsg.msg = confirmationMsg;
			outMsg.type = 0;
			
			// Notify all 
			outMsg.notifyAll();
		}
	}

	public void run() {
		// TODO Auto-generated method stub
		// Initialize datagram messages variables
		DMessagesSched = new DatagramMessages();
		DMessagesFlr = new DatagramMessages();
		
		// Create Data Socket Listener for listening to scheduler

		DSListenerScheduler = new DataSocketListener(1030, DMessagesSched, outMsg, 0, 1025, inMsg, SchedIP);
		
		// Create Data Socket Listener for listening to elevator
		DSListenerFloors = new DataSocketListener(1027, DMessagesFlr, outMsg, 1, 1028, inMsg, FloorIP);
		
		// Create Thread for Data Socket Listener for Scheduler
		Thread DSListenerSchedulerThr = new Thread(DSListenerScheduler);
		
		// Create Thread for Data Socket Listener for Elevator
		Thread DSListenerFloorsThr = new Thread(DSListenerFloors);
		
		// Start Data Socket Listener for scheduler
		DSListenerSchedulerThr.start();
		
		// Start Data Socket Listener for elevator
		DSListenerFloorsThr.start();
		now = LocalDateTime.now();
		now.minus(offset, ChronoUnit.MILLIS);
		String Log = dtf.format(now) + ":Elevator:: Initial startup complete";
		sendLog(Log);
		System.out.println(Log);
		
		// Wait for number of floors and elevators
		while(floors == -1) {
			// Check if scheduler has sent some messages
			checkMessages();//Sched();
			
			// Check if number of floors was specified
//			if (!(floors == -1)) {
//				try {
//					Thread.sleep(500);
//				} catch (InterruptedException e) {
//					
//				}
//			}
		}
		now = LocalDateTime.now();
		now.minus(offset, ChronoUnit.MILLIS);
		Log = dtf.format(now) + ":Elevator:: Data received from Scheduler";
		sendLog(Log);
		System.out.println(Log);
		
		elevators = new Elevator[elevs];
		Thread[] elevatorThreads = new Thread[elevs];
		for(int i=0; i<elevs;i++) {
			elevators[i] = new Elevator(floors, i+1, o, SchedIP, FloorIP, LoggerIP, offset, LoggerPort);
			elevatorThreads[i] = new Thread(elevators[i]);
			elevatorThreads[i].start();
		}
		now = LocalDateTime.now();
		now.minus(offset, ChronoUnit.MILLIS);
		Log = dtf.format(now) + ":Elevator:: Elevator system and threads set up";
		sendLog(Log);
		System.out.println(Log);
		
		while(true) {
			// Check messages
			checkMessages();
			
			// Sleep
//			try {
//				Thread.sleep(500);
//			} catch (InterruptedException e) {
//				
//			}
		}
		
	}
	
	
	
	
//	public static void main(String[] args) {
//		
//		ElevatorSubsystem main = new ElevatorSubsystem();
//		Thread process = new Thread(main);
//		process.start();
//	}

}
