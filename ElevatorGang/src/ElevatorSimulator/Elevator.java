package ElevatorSimulator;
//Created by Matthew Fundarek 100941844

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.Arrays;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.TimeUnit;

import MasterSimulator.Overseer;

public class Elevator implements Runnable{
	private int state = 0; // 0 is Idle, 1 is Up and 2 is Down
	private ElevatorLamp[] lamps; //Simulated floor lamps in the elevator
	private ElevatorButton[] buttons; //Simulated Buttons in the elevator
	private ElevatorMotor motor; //Simulated elevator motor
	private ElevatorDoor doors;  //Simulated Doors
	private int current = 1; // Current Floor
	private int destination = 0;  //Current Destination Floor
	private int elevatorNumber;
	private int arrivalFlag = 0; //Denotes for update whether to inform floor of arrival
	private int departureFlag = 0;//Denotes for update whether to inform floor doors are closed
	private DatagramSocket sendSocket;//Socket for sending messages to the scheduler
	private DatagramSocket sendSocket2;//Socket for sending messages to the floor
	private DatagramPacket sendPacket;
	private int errorCountUp = 0;
	private int oldstate = 0;
	private DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss.SSS");
	private LocalDateTime now;
	private Instant T[] = new Instant[1000];
	private int t = 0;
//	public LinkedBlockingQueue<Integer> newDest;
	boolean errorFlag = false;
	private Overseer o;
	private String FloorIP;
	private String SchedIP;
	private String LoggerIP;
	private long offset;
	private int LoggerPort;
	public boolean hardError = false;
	public boolean transientError = false;
	
	//Constructor for the object, initializes the elevator with enough lamps for each floor
	public Elevator(int floors, int number, Overseer over, String SIP, String FIP, String LIP, long newOffset, int newLP) {
		LoggerIP = LIP;
		LoggerPort = newLP;
//		newDest = newNewDest;
		FloorIP = FIP;
		SchedIP = SIP;
		lamps = new ElevatorLamp[floors];
		buttons = new ElevatorButton[floors];
		for (int i = 0 ; i < floors ; i++) {
			lamps[i] = new ElevatorLamp();
			buttons[i] = new ElevatorButton(i+1, SIP, LoggerIP, LoggerPort, offset);
		}
		motor = new ElevatorMotor();
		doors = new ElevatorDoor();
		elevatorNumber = number;
		try {
	         
	         sendSocket = new DatagramSocket();
	         sendSocket2 = new DatagramSocket();
	         
	      } catch (SocketException se) {   // Can't create the socket.
	    	  se.printStackTrace();
		      System.exit(1);
	      }
		o=over;
		offset = newOffset;
	}

	public void run() {
			
		while(true) {	
			try {
				operating();
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
	}
	
	public void isConfirmed(){
		
	}
	
	public void lightLamp(int Number) {
		//Toggles a given lamp light
		lamps[Number-1].toggleLamp();
		o.lampUpdate(elevatorNumber, Number, lamps[Number-1].checkLamp());
		now = LocalDateTime.now();
		now.minus(offset, ChronoUnit.MILLIS);
		String Log = dtf.format(now) + ":Elevator:: Elevator "+ (elevatorNumber) +" Floor light "+Number+" toggled  ";
		sendLog(Log);
		System.out.println(Log);
	}
	
	public void pressButton(int Number) {
		//Presses a given number button and lights that lamp
		T[t] = buttons[Number-1].pressButton(elevatorNumber);
		t++;
		printOutFile();
		
		lightLamp(Number);
		now = LocalDateTime.now();
		now.minus(offset, ChronoUnit.MILLIS);
		String Log = dtf.format(now) + ":Elevator:: Elevator "+ (elevatorNumber) +" Elevator button "+Number+" pressed  ";
		sendLog(Log);
		System.out.println(Log);
		
		
	}
	
	public void operateMotor() {
		//Shorter function to operate motor
		motor.toggleMotor();
		o.motorUpdate(elevatorNumber, motor.checkMotor());
	}
	
	public void setArrival() {
		arrivalFlag = 1;
	}
	
	public void setDeparture() {
		departureFlag = 1;
	}
	
	public void operateDoor() {
		//Shorter function to operate door
		doors.toggleDoor();
		o.doorUpdate(elevatorNumber, doors.checkDoor());
	}
	
	public void newDestination(int number) {
		//Changes the destination of the elevator
		destination = number;
		o.destUpdate(elevatorNumber, destination);
	}
	
	public void sendUpdate() {
		//Updates the scheduler on current status, and notifies floor on arrival and door close
		o.elevUpdate(elevatorNumber, state, current);
		byte[] update = new byte[6];
		byte[] floor = new byte[6];
		update[0] = 1;
		update[1] = (byte) elevatorNumber;
		update[2] = 1;
		update[3] = (byte) state;
		if(current<=10) {
			update[4] = 0;
			update[5] = (byte) current;
		}else {
			int four = current / 10;
			int five = current % 10;
			update[4] = (byte) four;
			update[5] = (byte) five;
		}
		if(arrivalFlag == 1) {
			floor[0] = 0;
			if(current<=10) {
				floor[1] = 0;
				floor[2] = (byte) current;
			}else {
				int one = current / 10;
				int two = current % 10;
				floor[1] = (byte) one;
				floor[2] = (byte) two;
			}
			floor[3] = (byte) elevatorNumber;
			floor[4] = 0;
			floor[5] = (byte) oldstate;
			
		}else if(departureFlag == 1) {
			floor[0] = 0;
			if(current<=10) {
				floor[1] = 0;
				floor[2] = (byte) current;
			}else {
				int one = current / 10;
				int two = current % 10;
				floor[1] = (byte) one;
				floor[2] = (byte) two;
			}
			floor[3] = (byte) elevatorNumber;
			floor[4] = 1;
			floor[5] = (byte) oldstate;
		}
		
		try {/**/
			sendPacket = new DatagramPacket(update, update.length, InetAddress.getByName(SchedIP), 1025);
		} catch (UnknownHostException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		now = LocalDateTime.now();
		now.minus(offset, ChronoUnit.MILLIS);
		String Log = dtf.format(now) + ":Elevator:: Elevator "+ (elevatorNumber) +" sending elevator status update ";
		System.out.println(Log);
		
		try {
	         sendSocket.send(sendPacket);
	      } catch (IOException e) {
	         e.printStackTrace();
	         System.exit(1);
	      }
		T[t] = Instant.now();
		t++;
		printOutFile();
		sendLog(Log);
		
		//while(!confirm);
		if(arrivalFlag == 1 || departureFlag ==1) {
			try {/**/
				sendPacket = new DatagramPacket(floor, floor.length, InetAddress.getByName(FloorIP), 1028);
			} catch (UnknownHostException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}/**/
			
			try {
		         sendSocket2.send(sendPacket);
		      } catch (IOException e) {
		         e.printStackTrace();
		         System.exit(1);
		      }
			//while(!confirm);
			
			
			arrivalFlag = 0;
			departureFlag = 0;
			
		}
		
		if (transientError) {
			Log = dtf.format(now) + ": Elevator 3's Motor has failed.  It will recover";
			sendLog(Log);
			System.out.println(Log);
			motorFail();
			resolvedError();
		}else if (hardError) {
			Log = dtf.format(now) + ":Elevator:: Elevator 1 has crashed between floors";
			sendLog(Log);
			System.out.println(Log);
			elevatorCrash();
		}
		
	}
	
	private void sendLog(String Log) {
		now = LocalDateTime.now();
		now.minus(offset, ChronoUnit.MILLIS);
		byte[] log = Log.getBytes(StandardCharsets.UTF_8);
		try {
			//sendPacket = new DatagramPacket(msg, msg.length, InetAddress.getByName("172.17.40.149"), 1025);
			sendPacket = new DatagramPacket(log, log.length, InetAddress.getByName(LoggerIP), LoggerPort);
		} catch (UnknownHostException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		try {
	         sendSocket.send(sendPacket);
	      } catch (IOException e) {
	         e.printStackTrace();
	         System.exit(1);
	      }
	}
	
	private void sendErrMsg(int Err) {
		DatagramSocket Out = null;
		try {
			Out = new DatagramSocket();
		} catch (SocketException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		byte[] msg = new byte[6];
		Arrays.fill(msg,(byte) 0);
		msg[0] = (byte) 3;
		msg[1] = (byte) elevatorNumber;
		msg[2] = (byte) Err;
		DatagramPacket outMessage = null;
		try {
			outMessage = new DatagramPacket(msg, msg.length, InetAddress.getByName(SchedIP) , 1025);
		} catch (UnknownHostException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
//		now = LocalDateTime.now();
//		System.out.println(dtf.format(now) + ": Transient Error Detected in Elevator "+elevatorNumber);
		String Log = dtf.format(now) + ":Elevator:: Transient Error Detected in Elevator "+(elevatorNumber);
		sendLog(Log);
		
		try {
			Out.send(outMessage);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Out.close();
	}
	
	public void elevatorCrash() {
		//Elevator cable has snapped and the elevator is lost
		o.errorUpdate(elevatorNumber, true);
		sendErrMsg(2);
		now = LocalDateTime.now();
		now.minus(offset, ChronoUnit.MILLIS);
		String Log = dtf.format(now) + ":Elevator:: Elevator "+(elevatorNumber)+" has crashed between floors";
		sendLog(Log);
		System.out.println(Log);
		while(true);
	}
	
	

	public void motorFail() {
		now = LocalDateTime.now();
		now.minus(offset, ChronoUnit.MILLIS);
		String Log = dtf.format(now) + ":Elevator:: Elevator "+(elevatorNumber)+"'s Motor has failed.  It will recover";
		sendLog(Log);
		System.out.println(Log);
		
		
		//Elevator Software has crashed, will take time to reboot
		transientError();
		
		try {
			TimeUnit.SECONDS.sleep(10);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		resolvedError();
	
	}

	public void elevatorTrapped() {
		//Elevator Motor has suffered total failure, elevator is stuck between floors
		o.errorUpdate(elevatorNumber, true);
		sendErrMsg(2);
		while(true);
	}
	
	public void transientError() {
		o.errorUpdate(elevatorNumber, true);
		sendErrMsg(1);
//		DatagramSocket Out = null;
//		try {
//			Out = new DatagramSocket();
//		} catch (SocketException e1) {
//			// TODO Auto-generated catch block
//			e1.printStackTrace();
//		}
//		byte[] msg = new byte[6];
//		Arrays.fill(msg,(byte) 0);
//		msg[0] = (byte) 3;
//		msg[1] = (byte) elevatorNumber;
//		msg[2] = (byte) 1;
//		DatagramPacket outMessage = null;
//		try {
//			outMessage = new DatagramPacket(msg, msg.length, InetAddress.getLocalHost() , 1025);
//		} catch (UnknownHostException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
		now = LocalDateTime.now();
		now.minus(offset, ChronoUnit.MILLIS);
		String Log = dtf.format(now) + ":Elevator:: Transient Error Detected in Elevator "+elevatorNumber;
		sendLog(Log);
		System.out.println(Log);
//		try {
//			Out.send(outMessage);
//		} catch (IOException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//		Out.close();
	}
	
	private void printOutFile() {
		// Declare local variables
				Path dir = Paths.get("");
				String absolutePath = dir.toAbsolutePath().toString() + File.separator + "Elevator " + elevatorNumber + " OutputFile.csv";
				
				
				// Write content to file
				try(FileWriter fwriter = new FileWriter(absolutePath)){
					// Write file headers
					fwriter.write("Sent Time\n");
					// Write floor events
					for (int i = 0; i < T.length; i++) {
						fwriter.write(T[i]+ "\n");
					}
				} catch (IOException e) {
					e.printStackTrace();
				}
    }
	
	public void resolvedError() {
//		DatagramSocket Out = null;
//		try {
//			Out = new DatagramSocket();
//		} catch (SocketException e1) {
//			// TODO Auto-generated catch block
//			e1.printStackTrace();
//		}
//		byte[] msg = new byte[6];
//		Arrays.fill(msg,(byte) 0);
//		msg[0] = (byte) 3;
//		msg[1] = (byte) elevatorNumber;
//		msg[2] = (byte) 0;
//		DatagramPacket outMessage = null;
//		try {
//			outMessage = new DatagramPacket(msg, msg.length, InetAddress.getLocalHost() , 1025);
//		} catch (UnknownHostException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
		now = LocalDateTime.now();
		now.minus(offset, ChronoUnit.MILLIS);
		String Log = dtf.format(now) + ":Elevator:: Error Resolved in Elevator "+(elevatorNumber);
		sendLog(Log);
		System.out.println(Log);
		sendErrMsg(0);
		o.errorUpdate(elevatorNumber, false);
//		try {
//			Out.send(outMessage);
//		} catch (IOException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//		Out.close();
	}
	
	
	
	public void operating() throws InterruptedException {
		//Standard operating loop logic for the elevator.  Updates scheduler on state, floor, or door change
		while(true) {
			System.out.print("");
			if(state == 1) {
				if(destination != 0) {
					//Destination exists, check for state change
					if(destination < current) {
						//Destination is below us, switch direction
						operateMotor();
						operateMotor();
						//operateMotor();
						state = 2;
						sendUpdate();
						now = LocalDateTime.now();
						now.minus(offset, ChronoUnit.MILLIS);
						String Log = dtf.format(now) + ":Elevator:: Elevator "+ (elevatorNumber) +" Elevator descending  ";
						sendLog(Log);
						System.out.println(Log);
					} else if (destination == current) {
						//Arrived at destination, stop motor and open the doors
						oldstate = 1;
						arrivalFlag = 1;
						operateMotor();
						if(lamps[current-1].checkLamp()) {
							lightLamp(current);
						}
						TimeUnit.SECONDS.sleep(1);
						operateDoor();
						destination = 0;
						state = 0;
						sendUpdate();
						oldstate = 0;
						now = LocalDateTime.now();
						now.minus(offset, ChronoUnit.MILLIS);
						String Log = dtf.format(now) + ":Elevator:: Elevator "+ (elevatorNumber) +" Elevator arrived, doors open.   ";
						sendLog(Log);
						System.out.println(Log);
					} else if (destination > current) {
						//Travel to next floor
						TimeUnit.SECONDS.sleep(10);
						current++;
						sendUpdate();
						now = LocalDateTime.now();
						now.minus(offset, ChronoUnit.MILLIS);
						String Log = dtf.format(now) + ":Elevator:: Elevator "+ (elevatorNumber) +" Travelled to next floor: "+current+" ";
						sendLog(Log);
						System.out.println(Log);
					}
				}else {
					// no destination, halt elevator
					operateMotor();
					state = 0;
					sendUpdate();
					String Log = "Elevator "+ (elevatorNumber) +" Emergency stop, no destination  ";
					sendLog(Log);
					System.out.println(Log);
				}
			}else if(state == 2) {
				if(destination != 0) {
					//Destination exists, check for state change
					if(destination > current) {
						//Destination is above us, switch direction
						operateMotor();
						operateMotor();
//						operateMotor();
						state = 1;
						sendUpdate();
						now = LocalDateTime.now();
						now.minus(offset, ChronoUnit.MILLIS);
						String Log = dtf.format(now) + ":Elevator:: Elevator "+ (elevatorNumber) +" Elevator ascending  ";
						sendLog(Log);
						System.out.println(Log);
					} else if (destination == current) {
						//Arrived at destination, stop elevator and open doors
						arrivalFlag = 1;
						oldstate = 2;
						operateMotor();
						if(lamps[current-1].checkLamp()) {
							lightLamp(current);
						}
						TimeUnit.SECONDS.sleep(1);
						operateDoor();
						destination = 0;
						state = 0;
						sendUpdate();
						oldstate = 0;
						now = LocalDateTime.now();
						now.minus(offset, ChronoUnit.MILLIS);
						String Log = dtf.format(now) + ":Elevator:: Elevator "+ (elevatorNumber) +" Elevator arrived, opened door  ";
						sendLog(Log);
						System.out.println(Log);
					} else if (destination < current) {
						//Travel to next floor
						TimeUnit.SECONDS.sleep(10);
						current--;
						sendUpdate();
						now = LocalDateTime.now();
						now.minus(offset, ChronoUnit.MILLIS);
						String Log = dtf.format(now) + ":Elevator:: Elevator "+ (elevatorNumber) +" Travelled to next floor: "+current+" ";
						sendLog(Log);
						System.out.println(Log);
					}
				}else {
					// no destination, halt elevator
					operateMotor();
					state = 0;
					sendUpdate();
					now = LocalDateTime.now();
					now.minus(offset, ChronoUnit.MILLIS);
					String Log = dtf.format(now) + ":Elevator:: Elevator "+ (elevatorNumber) +" Emergency stop, no destination  ";
					sendLog(Log);
					System.out.println(Log);
				}
			}else if(state == 0) {
				if(doors.checkDoor()) {
					//Doors are open, wait 10 seconds and then close them
					TimeUnit.SECONDS.sleep(10);
					departureFlag = 1;
					operateDoor();
					sendUpdate();
					now = LocalDateTime.now();
					now.minus(offset, ChronoUnit.MILLIS);
					String Log = dtf.format(now) + ":Elevator:: Elevator "+ (elevatorNumber) +" Doors are now shut  ";
					sendLog(Log);
					System.out.println(Log);
					
				}else {
					//Doors are closed, figure out next state
					if(destination > current) {
						//Destination above, proceed to Up state
						
						state = 1;
						operateMotor();
						sendUpdate();
						now = LocalDateTime.now();
						now.minus(offset, ChronoUnit.MILLIS);
						String Log = dtf.format(now) + ":Elevator:: Elevator "+ (elevatorNumber) +" Destination received, ascending  ";
						sendLog(Log);
						System.out.println(Log);
					} else if(destination < current && destination != 0) {
						//Destination below, proceed to down state
						
						state = 2;
						operateMotor();
						sendUpdate();
						now = LocalDateTime.now();
						now.minus(offset, ChronoUnit.MILLIS);
						String Log = dtf.format(now) + ":Elevator:: Elevator "+ (elevatorNumber) +" Destination received, descending  ";
						sendLog(Log);
						System.out.println(Log);
					} else if(destination == 0) {
					//Elevator continues to wait for instructions
					
						}
						
						
					/*}*/ else if(destination == current) {
						// new request at current floor, open doors
						operateDoor();
						destination = 0;
						arrivalFlag = 1;
						if(lamps[current-1].checkLamp()) {
							lightLamp(current);
						}
						sendUpdate();
						now = LocalDateTime.now();
						now.minus(offset, ChronoUnit.MILLIS);
						String Log = dtf.format(now) + ":Elevator:: Elevator "+ (elevatorNumber) +" Destination at current floor, opened doors  ";
						sendLog(Log);
						System.out.println(Log);
					}
				}
				
			}
		}
	}
	
}
