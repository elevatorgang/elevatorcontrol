package ElevatorSimulator;

import java.util.Stack;

/*****************************************************************************
 * Class Functions
 * - Constructors
 * 	- Default
 * - isEmpty
 * 	- Returns whether or not message stack is empty
 * - popMessage
 * 	- Returns the first message in the message stack
 * - pushMessage
 * 	- Pushes a new message onto the stack
 ****************************************************************************/
public class DatagramMessages {
	// Declare member variables
	private Stack<byte[]> messageStack;
	
	/*****************************************************************
	 * Constructor
	 ****************************************************************/
	public DatagramMessages(){
		messageStack = new Stack<byte[]>();
	}
	
	/*****************************************************************
	 * Function to check if message stack is empty
	 ****************************************************************/
	public boolean isEmpty() {
		return messageStack.isEmpty();
	}
	
	/*****************************************************************
	 * Function to return element from message stack
	 ****************************************************************/
	public byte[] popMessage() {
		return messageStack.pop();
	}
	
	/*****************************************************************
	 * Function to push an element onto the message stack
	 ****************************************************************/
	public void pushMessage(byte[] Element) {
		messageStack.push(Element);
	}
}
