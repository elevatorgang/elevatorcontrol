package FloorSimulator;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.nio.charset.StandardCharsets;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.Arrays;
import java.util.Date;
import java.util.Random;
import java.util.concurrent.ConcurrentLinkedQueue;

import MasterSimulator.Overseer;

/*****************************************************************************
 * Class Functions
 * - Constructors
 * 	- Takes Floor Number, Floor Type, Main Process ID and Floor Requests object as inputs
 * - Run
 * 	- Calls function to wait for floor request
 * - waitForFloorRequestMessage
 * 	- Waits for floor request from main process
 * - println
 * 	- Prints to standard output from asynchronous context
 ****************************************************************************/
public class FloorThread implements Runnable{
	// Declare class attributes
	private volatile boolean Discontinue = false;
	private int FloorType;
	private int FloorNum;
	private long timeWaitingUp;
	private long timeWaitingDown;
	private boolean UpPressed;
	private boolean DownPressed;
	private ConcurrentLinkedQueue<byte[]> FRequestsUp;
	private ConcurrentLinkedQueue<byte[]> FRequestsDown;
	private ConcurrentLinkedQueue<byte[]> InMessages;
	private ConcurrentLinkedQueue<byte[]> OutMessages;
	private ConcurrentLinkedQueue<byte[]> Elevators;
	private DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss.SSS");
	private LocalDateTime now = LocalDateTime.now();
	private Overseer o;
	private long offset;
	private String LoggerIP;
	private int LoggerPort;
	/*****************************************************************************
	 * Constructor
	 ****************************************************************************/
	public FloorThread(int newFloorNum, int newFloorType, ConcurrentLinkedQueue<byte[]> newInMsgs, ConcurrentLinkedQueue<byte[]> newOutMsgs, Overseer over, long newOffset, int LP, String LIP) {
		// Initialize attributes
		LoggerPort = LP;
		LoggerIP = LIP;
		FloorNum = newFloorNum;
		FloorType = newFloorType;
		timeWaitingUp = 0;
		timeWaitingDown = 0;
		UpPressed = false;
		DownPressed = false;
		FRequestsUp = new ConcurrentLinkedQueue<byte[]>();
		FRequestsDown = new ConcurrentLinkedQueue<byte[]>();
		InMessages = newInMsgs;
		OutMessages = newOutMsgs;
		Elevators = new ConcurrentLinkedQueue<byte[]>();
		o = over;
		offset = newOffset;
	}

	/*****************************************************************************
	 * Call function to wait for a request message on thread startup
	 ****************************************************************************/
	public void run() {
		// Check for new messages every 500 milliseconds
		while(!Discontinue) {
			// Check for messages
			WaitForMessages();
		}
	}
	
	/*****************************************************************************
	 * Function to wait for a request message
	 ****************************************************************************/
	private void WaitForMessages() {
		// Declare local variables
		ConcurrentLinkedQueue<byte[]> myMsgs;
		
		// Get lock on in messages
		synchronized(InMessages) {
			// Wait for in messages
			while(InMessages.isEmpty()) {
				try {
					InMessages.wait();
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			
			// Check if there is a new message 
			if (InMessages.isEmpty()) {
				return;
			}
			
			// Check if there message is for this floor
			myMsgs = new ConcurrentLinkedQueue<byte[]>();
			ConcurrentLinkedQueue<byte[]> notMyMsgs = new ConcurrentLinkedQueue<byte[]>();
			while(!InMessages.isEmpty()) {
				if (InMessages.peek()[1] * 10 + InMessages.peek()[2] == FloorNum) {
					// Add message to my messages
					myMsgs.add(InMessages.remove());
				} else {
					// Add message to not my messages
					notMyMsgs.add(InMessages.remove());
				}
			}
			
			// Put the messages in not my messages back into the message queue
			while(!notMyMsgs.isEmpty()) {
				InMessages.add(notMyMsgs.remove());
			}
		}
		
		// Process messages
		ProcessMessages(myMsgs);
	}
	
	private void ProcessMessages(ConcurrentLinkedQueue<byte[]> msgs) {
		// Process each message
		while (!msgs.isEmpty()) {
			if (msgs.peek()[0] == 0) {
				// Process Floor Request
				ProcessFloorRequest(msgs.remove());
			}else if (msgs.peek()[0] == 1) {
				// Process Elevator Arrival
				ProcessElevatorArrival(msgs.remove());
			}else if (msgs.peek()[0] == 2) {
				// Process Elevator Departure
				ProcessElevatorDeparture(msgs.remove());
			}
		}
	}
	
	private void ProcessElevatorDeparture(byte[] msg) {
		// Declare local variables
		int ElevNum = 0;
		boolean ElevFound = false;
		ConcurrentLinkedQueue<byte[]> tempElevs = new ConcurrentLinkedQueue<byte[]>();
		
		// Find elevator
		while(!Elevators.isEmpty()) {
			if (Elevators.peek()[0] == msg[3]) {
				Elevators.remove();
				
				// Increment elevator number
				ElevNum = ElevNum + 1;
				
				// Set elevator found to true
				ElevFound = true;
				
				break;
			} else {
				tempElevs.add(Elevators.remove());
			}
			// Increment elevator number
			ElevNum = ElevNum + 1;
		}
		
		// Add elevators back in
		while(!tempElevs.isEmpty()) {
			Elevators.add(tempElevs.remove());
		}
		
		// If Elevator was found, send confirmation
		if (ElevFound) {
			SendElevEventConfirmation(ElevNum, msg[4], msg[5]);
		}
	}
	
	private void SendElevEventConfirmation(int Elevator, int ArrDep, int Dir) {
		// Declare local variables
		byte[] msg = new byte[6];
		
		// Fill message
		msg[0] = 8;
		msg[1] = (byte)((FloorNum - FloorNum%10)/10);
		msg[2] = (byte)(FloorNum%10);
		msg[3] = (byte)Elevator;
		msg[4] = (byte)ArrDep;
		msg[5] = (byte)Dir;
		now = LocalDateTime.now();
		now.minus(offset, ChronoUnit.MILLIS);
		String Log;
		if (ArrDep == 0 && Dir == 0) {
			Log = dtf.format(now) + ":Floor:: Sending confirmation of elevator " + Elevator + " arrival to floor " + FloorNum + " going up";
		} else if (ArrDep == 1 && Dir == 0) {
			Log = dtf.format(now) + ":Floor:: Sending confirmation of elevator " + Elevator + " departure from floor " + FloorNum + " going up";
		} else if (ArrDep == 0 && Dir == 1) {
			Log = dtf.format(now) + ":Floor:: Sending confirmation of elevator " + Elevator + " arrival to floor " + FloorNum + " going down";
		} else if (ArrDep == 1 && Dir == 1) {
			Log = dtf.format(now) + ":Floor:: Sending confirmation of elevator " + Elevator + " departure from floor " + FloorNum + " going down";
		}else {
			Log = "";//dtf.format(now) + ": Error: What the hell am I sending";
		}
		sendLog(Log);
		println(Log);
		
		// Add message to out messages
		synchronized(OutMessages) {
			OutMessages.add(msg);
		}
	}
	
	private void sendLog(String Log) {
		now = LocalDateTime.now();
		now.minus(offset, ChronoUnit.MILLIS);
		byte[] log = Log.getBytes(StandardCharsets.UTF_8);
		DatagramSocket sendSocket;
		DatagramPacket sendPacket;
		try {
			
			//sendPacket = new DatagramPacket(msg, msg.length, InetAddress.getByName("172.17.40.149"), 1025);
			sendPacket = new DatagramPacket(log, log.length, InetAddress.getByName(LoggerIP), LoggerPort);
			sendSocket = new DatagramSocket(1041);
	         sendSocket.send(sendPacket);
	         sendSocket.close();
		} catch (UnknownHostException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	private void ProcessElevatorArrival(byte[] msg) {
		// Get current time
		Date currDate = new Date();
		long currTime = currDate.getTime();
		
		// Prepare new elevator bit string
		byte[] newElev = new byte[2];
		newElev[0] = msg[3]; //Elevator
		if(msg[3]>9 || msg[2]>9 || newElev[0]>9) {
			println("Error");
		}
		
		// Check which direction the elevator is going
		if (msg[5] == 1 || (msg[5] == 0 && !FRequestsUp.isEmpty())) {
			now = LocalDateTime.now();
			now.minus(offset, ChronoUnit.MILLIS);
			// Print time to complete up requests
			String Log = dtf.format(now) + ":Floor:: Time to complete requests was: " + Long.toString(currTime - timeWaitingUp);
			sendLog(Log);
			println(Log);
			
			// Add Elevator to Elevator Queue
			newElev[1] = 0;
			Elevators.add(newElev);
			
			// Reset time
			timeWaitingDown = currTime;
			
			// Turn off direction lamp
			UpPressed = false;
			
			o.upUpdate(FloorNum, UpPressed);
			
			// Send all button presses for going up
			while(!FRequestsUp.isEmpty()) {
				byte[] req = FRequestsUp.peek();
				int Destination = req[0] * 10 + req[1];
				SendButtonPressEvent(newElev[0], FRequestsUp.peek()[0] * 10 + FRequestsUp.peek()[1]);
				if (req[3] == 0) {
					SendError(newElev[0],0,Destination);
				}else if (req[3] == 1) {
					SendError(newElev[0],1,Destination);
				}
				// Remove request
				FRequestsUp.remove();
			}
			
			// Send elevator arrival confirmation
			SendElevEventConfirmation(newElev[0], msg[4], msg[5]);
		} else if (msg[5] == 0 || (msg[5] == 2 && !FRequestsDown.isEmpty())) {
			now = LocalDateTime.now();
			now.minus(offset, ChronoUnit.MILLIS);
			// Print time to complete up requests
			String Log = dtf.format(now) +"Time to complete requests was: " + Long.toString(currTime - timeWaitingDown);
			sendLog(Log);
			println(Log);

			// Add Elevator to Elevator Queue
			newElev[1] = 1;
			Elevators.add(newElev);
			
			// Turn off direction lamp
			DownPressed = false;

			o.downUpdate(FloorNum, DownPressed);
			
			// Reset time
			timeWaitingDown = currTime;
			
			// Send all button presses for going down
			while(!FRequestsDown.isEmpty()) {
				byte[] req = FRequestsDown.peek();
				int Destination = req[0] * 10 + req[1];
				SendButtonPressEvent(msg[3], FRequestsDown.peek()[0] * 10 + FRequestsDown.peek()[1]);
				if (req[3] == 0) {
					SendError(newElev[0],0,Destination);
				}else if (req[3] == 1) {
					SendError(newElev[0],1,Destination);
				}
				// Remove request
				FRequestsDown.remove();
			}
			
			// Send elevator arrival confirmation
			SendElevEventConfirmation(newElev[0], msg[4], msg[5]);
		}else {
			// Print time to complete up requests
			now = LocalDateTime.now();
			now.minus(offset, ChronoUnit.MILLIS);
			String Log = dtf.format(now) + ":Floor:: Time to complete requests was: " + Long.toString(currTime - timeWaitingDown);
			sendLog(Log);
			println(Log);

			// Add Elevator to Elevator Queue
			newElev[1] = 2;
			Elevators.add(newElev);
			
			// Turn off direction lamp
			DownPressed = false;

			o.downUpdate(FloorNum, DownPressed);
			
			// Reset time
			timeWaitingDown = currTime;
		}
	}
	
	private void ProcessFloorRequest(byte[] msg) {
		// Declare variables
		int Destination = msg[3] * 10 + msg[4];
		int Direction;
		byte[] FloorRequest = new byte[4];
		
		// Fill destination field of Floor request
		FloorRequest[0] = msg[3];
		FloorRequest[1] = msg[4];
		FloorRequest[3] = msg[5];
		
		// Check if direction is up
		if (Destination > FloorNum) {
			// Fill direction field of floor request
			FloorRequest[2] = 0;
			
			// Check if elevator is here
			int ElevNum = ElevatorHere(0);
			if (ElevNum != -1) {
				// Send button press event
				SendButtonPressEvent(ElevNum, Destination);
				if (FloorRequest[3] == 0) {
					SendError(ElevNum,0,Destination);
				}else if (FloorRequest[3] == 1) {
					SendError(ElevNum,1,Destination);
				}
			} else {
				// Store Floor Request

				FRequestsUp.add(FloorRequest);
				
				// Check direction lamp is on
				if (TurnOnDirLamp(0) == false) {
					// Send elevator arrival confirmation
					SendFloorEventConfirmation(FloorNum, Destination);
				}
			}
			
//			// Send elevator arrival confirmation
//			SendFloorEventConfirmation(FloorNum, Destination);
		} else if (Destination < FloorNum) {
			// Fill direction field of floor request
			FloorRequest[2] = 1;
			
			// Check if elevator is here
			int ElevNum = ElevatorHere(1);
			if (ElevNum != -1) {
				// Send button press event
				SendButtonPressEvent(ElevNum, Destination);
				if (FloorRequest[3] == 0) {
					SendError(ElevNum,0,Destination);
				}else if (FloorRequest[3] == 1) {
					SendError(ElevNum,1,Destination);
				}
			} else {
				// Store Floor Request

				FRequestsDown.add(FloorRequest);

				// Check direction lamp is on
				if (TurnOnDirLamp(1) == false) {
					// Send elevator arrival confirmation
					SendFloorEventConfirmation(FloorNum, Destination);
				}
			}
			
//			// Send elevator arrival confirmation
//			SendFloorEventConfirmation(FloorNum, Destination);
		} else {
			// Print error
			now = LocalDateTime.now();
			now.minus(offset, ChronoUnit.MILLIS);
			String Log = dtf.format(now) + ":Floor:: Error: Floor Request Destination is the Origin Floor";
			sendLog(Log);
			println(Log);
		}
	}
	
	private void SendFloorEventConfirmation(int Origin, int Destination) {
		// Declare local variables
		byte[] msg = new byte[5];
		
		// Fill message
		msg[0] = 0;
		msg[1] = (byte)((Origin - Origin % 10)/10);
		msg[2] = (byte)(Origin%10);
		msg[3] = (byte)((Destination - Destination % 10)/10);
		msg[4] = (byte)(Destination%10);
		
		// Send confirmation message
		synchronized (OutMessages) {
			OutMessages.add(msg);
		}
	}
	
	private int ElevatorHere(int Direction) {
		// Declare local variables
		ConcurrentLinkedQueue<byte[]> TempElevs = new ConcurrentLinkedQueue<byte[]>();
		int ret = -1;
		
		// Loop through elevators here
		while(!Elevators.isEmpty()) {
			// Check if elevator is going in the same direction
			if (Elevators.peek()[1] == Direction) {
				ret =  Elevators.peek()[0];// * 10 + Elevators.peek()[1];
				break;
			} else {
				TempElevs.add(Elevators.remove());
			}
		}
		
		// Reinsert elevators into queue
		while (!TempElevs.isEmpty()) {
			Elevators.add(TempElevs.remove());
		}
		
		// Return match
		return ret;
	}
	
	private void SendError(int ElevNum, int ErrType, int Destination) {
		byte[] msg = new byte[6];
		Arrays.fill(msg, (byte)0);
		msg[0] = (byte)9;
		msg[1] = (byte)ErrType;
		msg[3] = (byte)ElevNum;
		
		now = LocalDateTime.now();
		now.minus(offset, ChronoUnit.MILLIS);
		String Log = dtf.format(now) + ":Floor:: Sending elevator error to elevator "+ ElevNum + " from button press event on floor "+FloorNum+" to go to " + Destination;
		sendLog(Log);
		println(Log);
		
		// Get out messages lock
		synchronized (OutMessages) {
			// Add floor request
			OutMessages.add(msg);
			
			// Notify all
			OutMessages.notifyAll();
		}
	}
	
	private void SendButtonPressEvent(int ElevNum, int Destination) {
		// Declare local variables
		byte[] msg = new byte[6];
		
		// Prepare message
		msg[0] = 7;
		msg[1] = (byte)((FloorNum - FloorNum%10)/10);
		msg[2] = (byte)(FloorNum%10);
		msg[3] = (byte)((Destination - Destination % 10)/10);
		msg[4] = (byte)(Destination % 10);
		msg[5] = (byte) ElevNum;
		
		now = LocalDateTime.now();
		now.minus(offset, ChronoUnit.MILLIS);
		String Log = dtf.format(now) + ":Floor:: Sending button press to elevator "+ ElevNum + " to go to floor: " + Destination;
		sendLog(Log);
		println(Log);
		
		// Get out messages lock
		synchronized (OutMessages) {
			// Add floor request
			OutMessages.add(msg);
			
			// Notify all
			OutMessages.notifyAll();
		}
	}
	
	private boolean TurnOnDirLamp(int Direction) {
		// Get current time
		Date currDate = new Date();
		long currTime = currDate.getTime();
		
		// Set corresponding direction lamp on
		if (Direction == 0 && UpPressed == false) {
			UpPressed = true;
			o.upUpdate(FloorNum, UpPressed);
			timeWaitingUp = currTime;
			return false;
		} else if (Direction == 1 && DownPressed == false) {
			DownPressed = true;
			o.downUpdate(FloorNum, DownPressed);
			timeWaitingDown = currTime;
			return false;
		}
		return true;
	}
	
	public void discontinue() {
		// Set exit boolean to true
		Discontinue = true;
	}
	
	/*****************************************************************************
	 * Used by threads to print to standard output
	 * 	- Remove IlleagalMonitorState error
	 ****************************************************************************/
	public void println(String x) {
		// Print synchronized version of println for string
	    synchronized (this) {
	        System.out.print(x + "\n");
	    }
	}
}
