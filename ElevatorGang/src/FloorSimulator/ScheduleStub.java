package FloorSimulator;

import java.io.IOException;

import java.io.UnsupportedEncodingException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.util.Arrays;
import java.util.LinkedList;

/*****************************************************************************
 * Class Functions
 * - Constructors
 * 	- Default
 * - ProcessSendPacket
 * - SendThePacket
 * - println
 ****************************************************************************/
public class ScheduleStub implements Runnable{
	private DatagramSocket SchedulerSendReceivePort;
	private int port;
	private int destPort;
	private int NumFloors;
	private LinkedList<byte[]> FReqs;
	
	public ScheduleStub(int newPort, int newDestPort) {
		/***************************************************************
		 * Testing start
		 */
		port = newPort;
		destPort = newDestPort;
	}
	
	//*********************************************************************
	//*********************************************************************
	//*********************************************************************
	// Declare function to wait for packet
	public  byte[] WaitForPacket(DatagramSocket receiveSocket) {
		// Declare receive packet
		byte[] data = new byte[5];
		DatagramPacket receivePacket = new DatagramPacket(data, data.length);
		
		// Wait for return packet
		try {
			receiveSocket.receive(receivePacket);
		} catch (IOException e) {
//				e.printStackTrace();
//				println("SchedulerStub: Timeout, resending message");
			Arrays.fill(data, (byte)(-1));
			return data;
		}

		print("SchedulerStub: Packet received: ");
//				if (PName != "Client") {
		printBMessage(data, data.length);
//				}
		
		// Return packet received
		return data;
	}
	

	static public DatagramPacket ProcessSendPacket(byte[] sendByteSeq, int Port) {
		// Declare send packet
		byte[] data = new byte[5];
		DatagramPacket sendPacket = new DatagramPacket(data, data.length);
		
		// Format send packet
		try {
			sendPacket = new DatagramPacket(sendByteSeq, sendByteSeq.length, 
					InetAddress.getLocalHost(), Port);
		} catch (UnknownHostException e) {
			e.printStackTrace();
			System.exit(1);
		}
		
		// Return send datagram packet
		return sendPacket;
	}
	
	static public boolean SendThePacket(DatagramSocket sendReceiveSocket, DatagramPacket Packet) {
		try {			
			// Send the packet
			sendReceiveSocket.send(Packet);
		} catch (IOException e) {
			e.printStackTrace();
			System.exit(1);
		}
		// Return success
		return true;
	}
	/************************************************************************
	 * Testing end
	 * @param args
	 */

	/*****************************************************************************
	 * Used by threads to print to standard output
	 * 	- Remove IlleagalMonitorState error
	 ****************************************************************************/
	public void println(String x) {
	    synchronized (this) {
	        System.out.print(x + "\n");
	    }
	}
	
	public void print(String x) {
	    synchronized (this) {
	        System.out.print(x);
	    }
	}
	
	/*****************************************************************************
	 * Print byte array message
	 ****************************************************************************/
	private static void printBMessage(byte[] msg, int length) {
		for (int i = 0; i < length; i++) {
			System.out.print(msg[i]);
		}
		System.out.print("\n");
	}
	
	private boolean ReadMessage(byte[] msg) {
		// Check if message is initialization
		if (msg[0] == 0) {
			if (msg[1] * 10 + msg[2] == NumFloors) {
				println("SchedulerStub: Successfully initialized the specified number of floors: " + Integer.toString(NumFloors) +
						" vs " + Integer.toString(msg[1]) + Integer.toString(msg[2]));
				return true;
			}else {
				println("SchedulerStub: Failed to initialize the specified number of floors: " + Integer.toString(NumFloors) +
						" vs " + Integer.toString(msg[1]) + Integer.toString(msg[2]));
			}
		} // Check if message is check in
		else if (msg[0] == 1) {
			if (msg[1] == 0 && msg[2] == 1 && msg[3] == 0) {
				println("SchedulerStub: Successfully checked in");
			} else {
				println("SchedulerStub: Failed to properly check in");
			}
		} // Check if message is Floor Request
		else if (msg[0] == 2) {
			println("SchedulerStub: Received Floor Request: " + Integer.toString(msg[1]) + Integer.toString(msg[2]) + " (Floor) " + 
					Integer.toString(msg[3]) + " (Direction)");
		}
		 return false;
//			LinkedList<byte[]> tempFReqs = new LinkedList<byte[]>();
//			boolean FReqFound = false;
//			
//			while (!FReqs.isEmpty()) {
//				if (FReqs.peek()[0] == msg[1] && FReqs.peek()[1] == msg[2] && FReqs.peek()[2] == msg[3]) {
//					FReqFound = true;
//					println("Floor Request successfully processed");
//					break;
//				}else {
//					tempFReqs.add(FReqs.remove());
//				}
//			}
//			
//			while (!tempFReqs.isEmpty()) {
//				FReqs.add(tempFReqs.remove());
//			}
	}

	@Override
	public void run() {
		byte[] data = new byte[4];
		
		try {
			SchedulerSendReceivePort = new DatagramSocket(port);
		} catch (SocketException e) {
			e.printStackTrace();
		}
		
		// Send init
		boolean FSInitialized = false;
		while(!FSInitialized) {
			// Prepare init packet for floor subsystem
			data[0] = (byte)0;
			data[1] = (byte)0;
			data[2] = (byte)2;
			data[3] = (byte)0;
			NumFloors = data[1] * 10 + data[2];
			
			// Set time out for listening for confirmation packet
//			try {
//				SchedulerSendReceivePort.setSoTimeout(4000);
//			} catch (SocketException e) {
//				// TODO Auto-generated catch block
//				e.printStackTrace();
//			}
						
			// Send the init packet to the floor subsystem
			DatagramPacket sendPacket = ProcessSendPacket(data, destPort);
			SendThePacket(SchedulerSendReceivePort, sendPacket);
			
			// Wait for confirmation
			byte[] response = WaitForPacket(SchedulerSendReceivePort);
			FSInitialized = ReadMessage(response);
			// Check if initialization was confirmed
//			if (response[0] != (byte)(-1)) {
//				FSInitialized = true;
//			}
		}
		int numCheckIns = 0;
		// Wait for floor requests
		while(true) {
			// Pring port waiting on
//			println("SchedulerStub: Waiting on Port: " + SchedulerSendReceivePort.getLocalPort());
			
			if (numCheckIns < 20) {
				// Send check in
				byte[] checkIn = new byte[4];
				checkIn[0] = 1;
				checkIn[1] = 0;
				checkIn[2] = 0;
				checkIn[3] = 0;
				DatagramPacket sendCIPacket = ProcessSendPacket(checkIn, destPort);
				SendThePacket(SchedulerSendReceivePort, sendCIPacket);
				numCheckIns = numCheckIns + 1;
			}
			// Wait for confirmation
			byte[] response = WaitForPacket(SchedulerSendReceivePort);
			
			// return appropriate response
			if(response[0] == 2) {
				// Store floor number and direction
				int FloorNum = response[1] * 10 + response[2];
				int dir = response[3];
				
				ReadMessage(response);
//				// Print message received
//				if (dir == 0) {
//					println("SchedulerStub: Received request for floor: " + Integer.toString(FloorNum) + ", Direction: Down");
//				} else if (dir == 1) {
//					println("SchedulerStub: Received request for floor: " + Integer.toString(FloorNum) + ", Direction: Up");
//				}
				
				// Prepare response
				byte[] reply = new byte[4];
				reply[0] = (byte)3;
				reply[1] = (byte)response[1];
				reply[2] = (byte)response[2];
				reply[3] = (byte)response[3];
				
				// Send response
				DatagramPacket sendPacket = ProcessSendPacket(reply, destPort);
				SendThePacket(SchedulerSendReceivePort, sendPacket);
			}
		}
		
//		for (int j = 0; j < 19; j++) {
//			data[0] = (byte)0;
//			data[1] = (byte)0;
//			data[2] = (byte)2;
//			data[3] = (byte)1;
//			data[4] = (byte)3;
//			
//			DatagramPacket sendPacket = ProcessSendPacket(data, destPort);
//			
//			try {
//				Thread.sleep(500);
//			} catch (InterruptedException e) {
//				e.printStackTrace();
//			}
//			SendThePacket(SchedulerSendReceivePort, sendPacket);
//			WaitForPacket(SchedulerSendReceivePort);
//		}
	}
}
