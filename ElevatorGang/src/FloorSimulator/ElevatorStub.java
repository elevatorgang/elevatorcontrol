package FloorSimulator;

import java.io.IOException;

import java.io.UnsupportedEncodingException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;
import java.net.UnknownHostException;

/*****************************************************************************
 * Class Functions
 * - Constructors
 * 	- Default
 * - ProcessSendPacket
 * - SendThePacket
 * - println
 ****************************************************************************/
public class ElevatorStub implements Runnable{
	private DatagramSocket SchedulerSendReceivePort;
	private int port;
	private int destPort;
	
	public ElevatorStub(int newPort, int newDestPort) {
		/***************************************************************
		 * Testing start
		 */
		port = newPort;
		destPort = newDestPort;
	}
	
	//*********************************************************************
	//*********************************************************************
	//*********************************************************************
	// Declare function to wait for packet
	public DatagramPacket WaitForPacket(DatagramSocket receiveSocket) {
		// Declare receive packet
		byte[] data = new byte[6];
		DatagramPacket receivePacket = new DatagramPacket(data, data.length);
		
		// Wait for return packet
		try {
			receiveSocket.receive(receivePacket);
		} catch (IOException e) {
			e.printStackTrace();
			return null;
		}

		System.out.println("ElevatorStub: Packet received:");

		printBMessage(data, data.length);
		
		// Return packet received
		return receivePacket;
	}
	

	static public DatagramPacket ProcessSendPacket(byte[] sendByteSeq, int Port) {
		// Declare send packet
		byte[] data = new byte[6];
		DatagramPacket sendPacket = new DatagramPacket(data, data.length);
		
		// Format send packet
		try {
			sendPacket = new DatagramPacket(sendByteSeq, sendByteSeq.length, 
					InetAddress.getLocalHost(), Port);
		} catch (UnknownHostException e) {
			e.printStackTrace();
			System.exit(1);
		}
		
		// Return send datagram packet
		return sendPacket;
	}
	
	static public boolean SendThePacket(DatagramSocket sendReceiveSocket, DatagramPacket Packet) {
		try {
			// Send the packet
			sendReceiveSocket.send(Packet);
		} catch (IOException e) {
			e.printStackTrace();
			System.exit(1);
		}
		// Return success
		return true;
	}
	/************************************************************************
	 * Testing end
	 * @param args
	 */

	/*****************************************************************************
	 * Used by threads to print to standard output
	 * 	- Remove IlleagalMonitorState error
	 ****************************************************************************/
	public void println(String x) {
	    synchronized (this) {
	        System.out.print(x + "\n");
	    }
	}
	
	/*****************************************************************************
	 * Print byte array message
	 ****************************************************************************/
	private static void printBMessage(byte[] msg, int length) {
		for (int i = 0; i < length; i++) {
			System.out.print(msg[i]);
		}
		System.out.print("\n");
	}
	
	private boolean ReadMessage(byte[] msg) {
		if (msg[0] == 1 && msg[4] == 0 && msg[5] == 0) {
			println("Elevator Stub: Confirmed Elevator arrival: " + Integer.toString(msg[4]) + " (Arrival) " + Integer.toString(msg[5]) + " (Up) " +
					Integer.toString(msg[1]) + Integer.toString(msg[2]) + " (Floor) " + Integer.toString(msg[3]) + " (Elevator)");
		}else if (msg[0] == 1 && msg[4] == 0 && msg[5] == 1) {
			println("Elevator Stub: Confirmed Elevator arrival: " + Integer.toString(msg[4]) + " (Arrival) " + Integer.toString(msg[5]) + " (Down) " +
					Integer.toString(msg[1]) + Integer.toString(msg[2]) + " (Floor) " + Integer.toString(msg[3]) + " (Elevator)");
		}else if (msg[0] == 1 && msg[4] == 1 && msg[5] == 0) {
			println("Elevator Stub: Confirmed Elevator departure: " + Integer.toString(msg[4]) + " (Departure) " + Integer.toString(msg[5]) + " (Up) " +
					Integer.toString(msg[1]) + Integer.toString(msg[2]) + " (Floor) " + Integer.toString(msg[3]) + " (Elevator)");
		}else if (msg[0] == 1 && msg[4] == 1 && msg[5] == 1) {
			println("Elevator Stub: Confirmed Elevator departure: " + Integer.toString(msg[4]) + " (Departure) " + Integer.toString(msg[5]) + " (Down) " +
					Integer.toString(msg[1]) + Integer.toString(msg[2]) + " (Floor) " + Integer.toString(msg[3]) + " (Elevator)");
		}else if (msg[0] == 1 && msg[4] == 0 && msg[5] == 2) {
			println("Elevator Stub: Confirmed Elevator arrival: " + Integer.toString(msg[4]) + " (Arrival) " + Integer.toString(msg[5]) + " (Idle) " +
					Integer.toString(msg[1]) + Integer.toString(msg[2]) + " (Floor) " + Integer.toString(msg[3]) + " (Elevator)");
		}else if (msg[0] == 1 && msg[4] == 1 && msg[5] == 2) {
			println("Elevator Stub: Confirmed Elevator departure: " + Integer.toString(msg[4]) + " (Departure) " + Integer.toString(msg[5]) + " (Idle) " +
					Integer.toString(msg[1]) + Integer.toString(msg[2]) + " (Floor) " + Integer.toString(msg[3]) + " (Elevator)");
		}else if (msg[0] == 0) {
			println("Elevator Stub: Received button press event: " + Integer.toString(msg[1]) + Integer.toString(msg[2]) + " (Origin Floor) " + 
					Integer.toString(msg[3]) + Integer.toString(msg[4]) + " (Destination Floor) " + Integer.toString(msg[5]) + " (Elevator)");
		}else {
			println("Elevator Stub: ERROR: No idea what message this is");
			return false;
		}
		return true;
	}

	@Override
	public void run() {
		byte[] data = new byte[6];
		
		try {
			SchedulerSendReceivePort = new DatagramSocket(port);
		} catch (SocketException e) {
			e.printStackTrace();
		}
		
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		// Pring port waiting on
		println("ElevatorStub: Waiting on Port: " + SchedulerSendReceivePort.getLocalPort());
		byte[] reply;
		
		//Send elevator arrival up
		data[0] = (byte)0;
		data[1] = (byte)0;
		data[2] = (byte)1;
		data[3] = (byte)4;
		data[4] = (byte)0;
		data[5] = (byte)0;
			
		DatagramPacket sendPacket = ProcessSendPacket(data, destPort);
		
		SendThePacket(SchedulerSendReceivePort, sendPacket);
		reply = WaitForPacket(SchedulerSendReceivePort).getData();
		ReadMessage(reply);
		
		//Send elevator departure up
		data[0] = (byte)0;
		data[1] = (byte)0;
		data[2] = (byte)1;
		data[3] = (byte)4;
		data[4] = (byte)1;
		data[5] = (byte)0;
		
		SendThePacket(SchedulerSendReceivePort, sendPacket);
		reply = WaitForPacket(SchedulerSendReceivePort).getData();
		ReadMessage(reply);
		
		//Send elevator arrival down
		data[0] = (byte)0;
		data[1] = (byte)0;
		data[2] = (byte)1;
		data[3] = (byte)4;
		data[4] = (byte)0;
		data[5] = (byte)1;
		
		SendThePacket(SchedulerSendReceivePort, sendPacket);
		reply = WaitForPacket(SchedulerSendReceivePort).getData();
		ReadMessage(reply);
		
		//Send elevator departure down
		data[0] = (byte)0;
		data[1] = (byte)0;
		data[2] = (byte)1;
		data[3] = (byte)4;
		data[4] = (byte)1;
		data[5] = (byte)1;
			
		sendPacket = ProcessSendPacket(data, destPort);
		
		SendThePacket(SchedulerSendReceivePort, sendPacket);
		reply = WaitForPacket(SchedulerSendReceivePort).getData();
		ReadMessage(reply);
		
		//Send elevator arrival idle
		data[0] = (byte)0;
		data[1] = (byte)0;
		data[2] = (byte)1;
		data[3] = (byte)4;
		data[4] = (byte)0;
		data[5] = (byte)2;
		
		SendThePacket(SchedulerSendReceivePort, sendPacket);
		reply = WaitForPacket(SchedulerSendReceivePort).getData();
		ReadMessage(reply);
		
		//Send elevator departure idle
		data[0] = (byte)0;
		data[1] = (byte)0;
		data[2] = (byte)1;
		data[3] = (byte)4;
		data[4] = (byte)1;
		data[5] = (byte)2;
		
		SendThePacket(SchedulerSendReceivePort, sendPacket);
		reply = WaitForPacket(SchedulerSendReceivePort).getData();
		ReadMessage(reply);
		
		while(true) {
			reply = WaitForPacket(SchedulerSendReceivePort).getData();
			ReadMessage(reply);	
		}
	}
}
