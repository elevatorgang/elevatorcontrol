package FloorSimulator;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.util.concurrent.ConcurrentLinkedQueue;

/*****************************************************************************
 * Class Functions
 * - Constructors
 * 	- Takes port, main process id and datagram new message object as inputs
 * - run
 * 	- Calls function to listen on port
 * - Listen
 * 	- Listens on port
 * 	- Calls Process Datagram function
 * - ProcessDatagram
 * 	- Processes the received datagram
 * 	- Sends message to main process
 * - println
 * 	- Prints to standard output from asynchronous context
 ****************************************************************************/
public class DataSocketListener implements Runnable{
	// Declare class attributes
	private int Port;
	private DatagramSocket Socket;
	private ConcurrentLinkedQueue<byte[]> msgs;
	private int Sender;
//	private LinkedBlockingQueue<Integer> msgs;
	
	/*****************************************************************************
	 * Constructor
	 * 	- Takes in:
	 * 		- Port: To listen for DatagramPackets on
	 * 		- Parent Process ID: To be able to notify main process of new messages
	 ****************************************************************************/
	public DataSocketListener(int newSender, int newPort, ConcurrentLinkedQueue<byte[]> newMsgs) {
		// Initialize class attributes
		Port = newPort;
		Sender = newSender;
		msgs = newMsgs;
		try {
			Socket = new DatagramSocket(Port);
		} catch (SocketException e) {
			e.printStackTrace();
		}
	}
	
	/*****************************************************************************
	 * Run
	 * 	- Starts DataSocket Listener thread:
	 ****************************************************************************/
	public void run() {
		// Print start message
		println("DataSocketListener: DatagramSocket Listener has started running. Listening on port: " + Integer.toString(Port));
		
		while(true) {
			// Listen on port
			Listen();
		}
	}
	
	/*****************************************************************************
	 * Listens on port for datagram packets
	 * 	- Listens for packet
	 * 	- Sends packet for processing
	 ****************************************************************************/
	private void Listen() {
		// Declare receive packet
		byte[] data;
		if (Sender == 1) {
			data = new byte[6];
		}else if (Sender == 0) {
			data = new byte[4];
		}else {
			return;
		}
		DatagramPacket receivePacket = new DatagramPacket(data, data.length);
		
		// Listen on port
		try {
			// Listen on port
			Socket.receive(receivePacket);
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		// Process DatagramPacket
		ProcessDatagram(receivePacket, data);
	}
	
	/*****************************************************************************
	 * Processes datagram packet received and prints out the contents
	 ****************************************************************************/
	private void ProcessDatagram(DatagramPacket receivePacket, byte[] data) {
		// Declare local variables
		int len = receivePacket.getLength();
		byte[] newMsg;
		
		// Set length of new in message
		if (Sender == 0) {
			newMsg = new byte[5];
		} else if (Sender == 1) {
			newMsg = new byte[7];
		}else {
			return;
		}
		
		if (receivePacket.getAddress() != null) {
			//print("DataSocketListener: Received packet containing: ");/**/
			
			// Store message in byte array
			data = receivePacket.getData();
			
			// Fill new message
			newMsg[0] = (byte)Sender;
			for (int i = 0; i < data.length; i++) {
				newMsg[i+1] = data[i];
			}
			
			// Print byte message
			//printBMessage(data, data.length);/**/
			
			// Notify Main Process Thread
//			msgs.add(msgs);
			synchronized (msgs) {
				// Push the message onto the message queue
				msgs.add(newMsg);
				
				// Notify Main Process
				msgs.notifyAll();
			}
		}
	}
	
	/*****************************************************************************
	 * Print byte array message
	 ****************************************************************************/
	private void printBMessage(byte[] msg, int length) {
		// Print each byte individually
		for (int i = 0; i < length; i++) {
			print(msg[i]);
		}
		// Print newline character
		print("\n");
	}
	
	/*****************************************************************************
	 * Used by threads to print to standard output
	 * 	- Remove IlleagalMonitorState error
	 ****************************************************************************/
	public void println(String x) {
		// Synchronized version of println for string
	    synchronized (this) {
	        System.out.print(x + "\n");
	    }
	}
	public void print(String x) {
		// Synchronized version of print for string
		synchronized (this) {
	        System.out.print(x);
	    }
	}
	public void print(Byte x) {
		// Synchronized version of print for byte
	    synchronized (this) {
	        System.out.print(x);
	    }
	}
}
