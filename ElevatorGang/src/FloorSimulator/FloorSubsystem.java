package FloorSimulator;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.Arrays;
import java.util.Date;
import java.util.Random;
import java.util.concurrent.ConcurrentLinkedQueue;

import FloorSimulator.DataSocketListener;
import MasterSimulator.Overseer;
import TimeSync.TimeSync;

import org.apache.commons.net.ntp.NTPUDPClient; 
import org.apache.commons.net.ntp.TimeInfo;//import org.apache.commons.net.ntp.NTPUDPClient;
//import org.apache.commons.net.ntp.NtpUtils;
//import org.apache.commons.net.ntp.NtpV3Packet;
//import org.apache.commons.net.ntp.TimeInfo;
//import org.apache.commons.net.ntp.TimeStamp;

/*****************************************************************************
 * Class Functions
 * - Constructors
 * 	- Default
 * - StartMainProces
 * 	- Initializes Datagram Socket listener and threads, as well as starting
 * the function to wait for message from scheduler
 * - SendFloorRequest
 * 	- Sends floor request to floors
 * - CheckMessages
 * 	- Checks messages from Datagram Socket Listener
 * - main
 * 	- Starts the main process thread
 * - println
 * 	- prints to standard output from asynchronous context
 * - run
 * 	-Calls function to start main process
 ****************************************************************************/
public class FloorSubsystem implements Runnable{
	/*****************************************************************************
	 * Class to hold events
	 *****************************************************************************/
	private class FloorEvents {
		// Declare class attributes
		public int time;
		public int origin;
		public int destination;
		public int direction;
		public int error;
		
		// Constructor
		FloorEvents(){
			
		}
	}
	
	/*****************************************************************************
	 * Public Variables
	 ****************************************************************************/
	private static int SchedInPort = 1026;
	private static int SchedOutPort = 1033;
	private static int ElevInPort = 1028;
	private static int OutPort = 1029;
	private static int NumEvents = 100;
	private int SchedID = 0;
	private int ElevID = 1;
	private String SchedIP;
	private String ElevIP;
	private String LoggerIP;

	// *********************************************************************
	// Comment out this line and give the FName variable the name of your 
	// events file to use your own events file
	// *********************************************************************
	private static String FName = "EventsFile.csv";
	private FloorThread[] FloorThreadObjs;
	private DataSocketListener DSListenerScheduler;
	private DataSocketListener DSListenerElevator;
	private DataSocketSender DSSender;
	private ConcurrentLinkedQueue<byte[]> InMessagesPorts;
	private ConcurrentLinkedQueue<byte[]> OutMsgsPorts;
	private ConcurrentLinkedQueue<byte[]> InMessagesFloors;
	private ConcurrentLinkedQueue<byte[]> OutMsgsFloors;
	private int NumFloors;
	private FloorEvents[] FEvents;
	private long startTime;
	private DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss.SSS");
	private LocalDateTime now = LocalDateTime.now();
	private LocalDateTime T[] = new LocalDateTime[1000];
	private int t = 0;
	private Overseer o;
	private int LoggerPort;
	private long offset;
	
	private void syncClock() {
		// NTP Clock Synchronization Test
		offset = TimeSync.getTimeOffset();
	}
	// TEST
//	private static ScheduleStub Scheduler;
//	private static ElevatorStub Elevator;
	
	/*****************************************************************************
	 * Constructor
	 ****************************************************************************/
	public FloorSubsystem(Overseer over, String EIP, String SIP, String LIP, int LP) {
		// Initialize attributes
		LoggerIP = LIP;
		LoggerPort = LP;
		SchedIP = SIP;
		ElevIP = EIP;
		InMessagesPorts = new ConcurrentLinkedQueue<byte[]>();
		OutMsgsPorts = new ConcurrentLinkedQueue<byte[]>();
		InMessagesFloors = new ConcurrentLinkedQueue<byte[]>();
		OutMsgsFloors = new ConcurrentLinkedQueue<byte[]>();
		DSListenerScheduler = new DataSocketListener(SchedID, SchedInPort, InMessagesPorts);
		DSListenerElevator = new DataSocketListener(ElevID, ElevInPort, InMessagesPorts);
		DSSender = new DataSocketSender(OutPort, SchedOutPort, ElevInPort-1, OutMsgsPorts, SchedIP, ElevIP);
		NumFloors = -1;
		FEvents = new FloorEvents[NumEvents];
		Date startDate = new Date();
		startTime = startDate.getTime();
		o = over;
		syncClock();
	}
	
	/*****************************************************************************
	 * Starts the Main Process thread's initial operations
	 * 	- Stores its (Main Process') process id
	 *  
	 ****************************************************************************/
//	public static void main(String[] args) {
//		// Create Floor Subsystem Object
//		FloorSubsystem FS = new FloorSubsystem();
	public void begin() {
		
		// Create Thread for Data Socket Listener for Scheduler
		Thread DSListenerSchedulerThr = new Thread(DSListenerScheduler);
		
		// Create Thread for Data Socket Listener for Elevator
		Thread DSListenerElevatorThr = new Thread(DSListenerElevator);
		
		// Create Thread for Data Socket Sender
		Thread DSSender = new Thread(this.DSSender);
		
		// Start Data Socket Listener for scheduler
		DSListenerSchedulerThr.start();
		
		// Start Data Socket Listener for elevator
		DSListenerElevatorThr.start();
		
		// Start Data Socket Sender
		DSSender.start();
		
		// *********************************************************************
		// Comment out the following lines (Up until /**...**/) to use the actual 
		// Scheduler and Elevator subsystems (Not use the testing stubs)
		// *********************************************************************
		// create test scheduler and elevator
//		Scheduler = new ScheduleStub(1025, 1026);
//		Elevator = new ElevatorStub(1027, 1028);
//		
//		// Create test scheduler and elevator threads
//		Thread SchedulerThr = new Thread(Scheduler);
//		Thread ElevatorThr = new Thread(Elevator);
//		
//		// Start running test scheduler and elevator threads
//		SchedulerThr.start();
//		ElevatorThr.start();
		/**********************************************************************/
		
		// Wait for number of floors
		while(NumFloors == -1) {
			WaitForInMessages(0);
		}
		
		// Create events file
		createEventFile(NumFloors, FName);
		
		// Read in events file
		ReadEventsFile(10, FName);
		
		// Begin running floor request events
		for (int i = 0; i < FEvents.length; i++) {
			// Get event
			FloorEvents currFEvent = FEvents[i];
			
			// Declare variable to give the go ahead to dispatcch event
			boolean isTime = false;
			
			// wait until it is time for event
			while(!isTime) {
				// Update isTime variable
				isTime = isTimeForEvent(currFEvent);
				
				// If it is not time then sleep until it is time
				if (!isTime) {
					// Get time differenc
					long timeDiff = getTimeDiff();
					
					// Wait until at most next floor event and check for in messsages
					WaitForInMessages(currFEvent.time - timeDiff);
				}
			}
			
			// Prepare floor request
			byte[] FReq = new byte[6];
			FReq[0] = 0;
			FReq[1] = (byte)((FEvents[i].origin - ( FEvents[i].origin % 10))/10);
			FReq[2] = (byte)( FEvents[i].origin % 10);
			FReq[3] = (byte)(( FEvents[i].destination - ( FEvents[i].destination % 10))/10);
			FReq[4] = (byte)( FEvents[i].destination % 10);
			FReq[5] = (byte)(FEvents[i].error);
			
			// Send floor request to floors
			 SendFRToFloors(FReq);
			// Wait for confirmation
			 WaitFloorMessages(1);
			
			// Send floor request to Scheduler
			// SendFloorRequest(FReq);
		}
		
		// Finished sending request
		LocalDateTime now = LocalDateTime.now();
		now.minus(offset, ChronoUnit.MILLIS);
		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss.SSS");
		String Log = dtf.format(now) + ":Floor:: Main Process: Finished event file requests";
		sendLog(Log);
		System.out.println(Log);
		
		while(true) {
			// Check messages
			 WaitForInMessages(0);
		}
	}
	
	private void sendLog(String Log) {
		now = LocalDateTime.now();
		now.minus(offset, ChronoUnit.MILLIS);
		byte[] log = Log.getBytes(StandardCharsets.UTF_8);
		DatagramSocket sendSocket;
		DatagramPacket sendPacket;
		try {
			
			//sendPacket = new DatagramPacket(msg, msg.length, InetAddress.getByName("172.17.40.149"), 1025);
			sendPacket = new DatagramPacket(log, log.length, InetAddress.getByName(LoggerIP), LoggerPort);
			sendSocket = new DatagramSocket(1047);
	         sendSocket.send(sendPacket);
	         sendSocket.close();
		} catch (UnknownHostException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	private void SendFRToFloors(byte[] msg) {
		// Get lock on floor in messages
		synchronized(InMessagesFloors) {
			// Add message
			InMessagesFloors.add(msg);
			
			// Notify all
			InMessagesFloors.notifyAll();
		}
	}
	
	private void SendFloorRequest(byte[] msg) {
		// Declare local variables
		byte[] newMsg = new byte[4];
		int origin = msg[1] * 10 + msg[2];
		int destination = msg[3] * 10 + msg[4];
		
		// fill floor request
		newMsg[0] = (byte)2;
		newMsg[1] = msg[1];
		newMsg[2] = msg[2];
		
		now = LocalDateTime.now();
		now.minus(offset, ChronoUnit.MILLIS);
		// Determine direction
		if (origin > destination) {
			newMsg[3] = (byte)0;
			String Log = dtf.format(now) + ":Floor:: Sending floor request to scheduler for floor " + origin + " going down";
			sendLog(Log);
			println(Log);
		}else if (origin < destination) {
			newMsg[3] = (byte)1;
			String Log = dtf.format(now) + ":Floor:: Sending floor request to scheduler for floor " + origin + " going up";
			sendLog(Log);
			println(Log);
		}else {
			return;
		}
		
		// Send Floor request to scheduler
		T[t] = LocalDateTime.now();
		t++;
		SendMessageToPort(newMsg, 0);
		printOutFile();
	}
	
	private void printOutFile() {
		// Declare local variables
				Path dir = Paths.get("");
				String absolutePath = dir.toAbsolutePath().toString() + File.separator + "FloorOutputFile.csv";
				
				// Write content to file
				try(FileWriter fwriter = new FileWriter(absolutePath)){
					// Write file headers
					fwriter.write("Sent Time\n");
					// Write floor events
					for (int i = 0; i < T.length; i++) {
						fwriter.write(T[i]+ "\n");
					}
				} catch (IOException e) {
					e.printStackTrace();
				}
    }
	
	private void SendMessageToPort(byte[] msg, int Sender) {
		// Declare local variables
		byte[] newMsg;
		
		// Set newMsg size according to sender
		if (Sender == 0) {
			newMsg = new byte[5];
		} else if (Sender == 1) {
			newMsg = new byte[7];
		} else {
			return;
		}
		
		// Fill new message
		newMsg[0] = (byte) Sender;
		for (int i = 0; i < newMsg.length-1; i++) {
			newMsg[i+1] = msg[i];
		}
		
		// Get lock on out message
		synchronized(OutMsgsPorts) {
			// Fill out message
			OutMsgsPorts.add(newMsg);
			
			// Notify all 
			OutMsgsPorts.notifyAll();
		}
	}
	
	private void WaitForInMessages(long TimeDiff) {
		// Declare variable to hold retrieved messages from the data socket listeners
		ConcurrentLinkedQueue<byte[]> msgs = new ConcurrentLinkedQueue<byte[]>();
		
		synchronized(InMessagesPorts) {
			if(InMessagesPorts.isEmpty()) {
				try {
					InMessagesPorts.wait(TimeDiff);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			
			// Check if we have received a message from the scheduler
			if (!InMessagesPorts.isEmpty()) {
				// Retrieve messages
				while(!InMessagesPorts.isEmpty()) {
					msgs.add(InMessagesPorts.remove());
				}
			}
		}
		
		// Process messages from scheduler
		if (!msgs.isEmpty()) {
			ProcessMessages(msgs);
		}
	}
	
	private  void WaitFloorMessages(int waitType) {
		// Declare local variables
		ConcurrentLinkedQueue<byte[]> msgs = new ConcurrentLinkedQueue<byte[]>();
		
		// Get lock on floor out messages and process floor out messages
		while (!ProcessFloorOutMessages(msgs, waitType)) {
			synchronized (OutMsgsFloors) {
				if (OutMsgsFloors.isEmpty() && waitType == 1) {
					try {
						OutMsgsFloors.wait(1000);
					} catch (InterruptedException e) {
						continue;
					}
				}else {// if (OutMsgsFloors.isEmpty() && waitType == 0) {
					while (!OutMsgsFloors.isEmpty()) {
						msgs.add(OutMsgsFloors.remove());
					}
				}
			}
			if (waitType == 1 && !ProcessFloorOutMessages(msgs, waitType)) {
				return;
			}
		} 
	}
	
	private boolean ProcessFloorOutMessages(ConcurrentLinkedQueue<byte[]> msgs, int WaitType) {
		// Declare local variables
		byte[] newMsg;
		
		// Iterate over each message
		while (!msgs.isEmpty()) {
			// Check if message is floor request confirmation
			if (msgs.peek()[0] == 0) {
				SendFloorRequest(msgs.remove());
				// Check if waitng for floor request confirmation
				if (WaitType == 1) {
					return true;
				}
			} // Check if message is button press event
			else if (msgs.peek()[0] == 7) {
				// Fill new message
				newMsg = new byte[6];
				newMsg[0] = 7;
				for (int i = 1; i < newMsg.length; i ++) {
					newMsg[i] = msgs.peek()[i];
				}
				
				// Send button press event
				SendMessageToPort(newMsg, 1);
				
				// Remove message
				msgs.remove();
				
			} else if (msgs.peek()[0] == 8) {
				newMsg = new byte[6];
				newMsg[0] = 8;
				for (int i = 1; i < newMsg.length; i ++) {
					newMsg[i] = msgs.peek()[i];
				}
				
				// Send confirmation of elevator event
				SendMessageToPort(newMsg, 1);
				
				// Remove message
				msgs.remove();
				
				// Check if waiting for elevator event confirmation
				if (WaitType == 0) {
					return true;
				}
			}else if (msgs.peek()[0] == 9) {
				newMsg = new byte[6];
				newMsg[0] = 9;
				for (int i = 1; i < newMsg.length; i ++) {
					newMsg[i] = msgs.peek()[i];
				}
				
				// Send confirmation of elevator event
				SendMessageToPort(newMsg, 1);
				
				// Remove message
				msgs.remove();
				
				// Check if waiting for elevator event confirmation
//				if (WaitType == 0) {
//					return true;
//				}
			}
		}
		return false;
	}
	
	private void SendInitConfirm() {
		// Declare local variables
		byte[] msg = new byte[4];
		
		// fill message
		msg[0] = 0;
		msg[1] = (byte)((NumFloors - NumFloors %10)/10);
		msg[2] = (byte)(NumFloors % 10);
		msg[3] = 0;
		
		now = LocalDateTime.now();
		now.minus(offset, ChronoUnit.MILLIS);
		String Log = dtf.format(now) + ":Floor:: Sending init confirmation to scheduler for creating " + NumFloors + " floors";
		sendLog(Log);
		println(Log);
		// Send confirmation message
		SendMessageToPort(msg, 0);
	}
	
	private void ProcessMessages(ConcurrentLinkedQueue<byte[]> msgs) {
		// Iterate over each message
		while(!msgs.isEmpty()) {
			now = LocalDateTime.now();
			now.minus(offset, ChronoUnit.MILLIS);
			if (msgs.peek().length == 5) {
				// Check if current message is initialization from the scheduler
				if (msgs.peek()[0] == 0 && msgs.peek()[1] == 0) {
					int numFloors = msgs.peek()[2] * 10 + msgs.peek()[3];
					
					// Initialize Floor Subsystem
					InitFS(msgs.remove());
					String Log = dtf.format(now) +": Received init from scheduler for " + numFloors + " floors";
					sendLog(Log);
					println(Log);
					
					// Send initialization confirmation
					SendInitConfirm();
				} // Check if current message is check in from the scheduler
				else if (msgs.peek()[0] == 0 && msgs.peek()[1] == 1) {
					// Send confirmation message to the scheduler
					sendConfirmationSched();
					String Log =dtf.format(now) +": Received check in from scheduler";
					sendLog(Log);
					println(Log);
					// Remove message
					msgs.remove();
				} // Check if current message is floor request processed by scheduler
				else if (msgs.peek()[0] == 0 && msgs.peek()[1] == 3) {
					int FloorNum = msgs.peek()[2] * 10 + msgs.peek()[3];
					int dir = msgs.peek()[4];
					
					if (dir == 0) {
						String Log = dtf.format(now) +": Received floor request processed confirmation from scheduler on floor " + FloorNum + " going up";
						sendLog(Log);
						println(Log);
					}else if (dir == 0) {
						String Log = dtf.format(now) +": Received floor request processed confirmation from scheduler on floor " + FloorNum + " going down";
						sendLog(Log);
						println(Log);
					}
					
					// Remove message
					msgs.remove();
				} 
			}else if (msgs.peek().length == 7) {
				// Check if current message is Elevator arrival or departure 
				if (msgs.peek()[0] == 1 && (msgs.peek()[5] == 0 || msgs.peek()[5] == 1)) {
					// Check if floor threads have been initialized
					if (NumFloors != -1) {
						int floorNum = msgs.peek()[2] * 10 + msgs.peek()[3];
						int elevNum = msgs.peek()[4];
						String Log;
						if (msgs.peek()[5] == 0 && msgs.peek()[6] == 1) {
							Log = dtf.format(now) +": Elevator " + elevNum + " has arrived at floor " + floorNum + " and is going up";
						}else if (msgs.peek()[5] == 1 && msgs.peek()[6] == 0) {
							Log = dtf.format(now) +": Elevator " + elevNum + " has departed from floor " + floorNum + " and is going up";
						}else if (msgs.peek()[5] == 0 && msgs.peek()[6] == 2) {
							Log = dtf.format(now) +": Elevator " + elevNum + " has arrived at floor " + floorNum + " and is going down";
						}else if (msgs.peek()[5] == 1 && msgs.peek()[6] == 1) {
							Log = dtf.format(now) +": Elevator " + elevNum + " has departed from floor " + floorNum + " and is going down";
						}else {
							Log = dtf.format(now) +": Elevator "+elevNum+" is idle on floor " + floorNum;
						}
						sendLog(Log);
						println(Log);
						// Notify floors of elevator arrival or departure
						SendMsgToFloors(msgs.remove());
						
						// Wait to hear confirmation from floor
						WaitFloorMessages(0);
					} else {
						now = LocalDateTime.now();
						now.minus(offset, ChronoUnit.MILLIS);
						String Log = dtf.format(now)+ ": Error: Elevator message received before floor threads have been initialized";
						sendLog(Log);
						println(Log);
						// Remove message
						msgs.remove();
					}
				} // Check if current message is Elevator confirming button press event
				else if (msgs.peek()[0] == 1 && msgs.peek()[1] == 1) {
					int origin = msgs.peek()[2] * 10 + msgs.peek()[3];
					int dest = msgs.peek()[4] * 10 + msgs.peek()[5];
					int elevNum = msgs.peek()[6];
					now = LocalDateTime.now();
					now.minus(offset, ChronoUnit.MILLIS);
					String Log = dtf.format(now) +": Elevator " + elevNum + " has confirmed button press from floor " + origin + " to go to floor " + dest;
					sendLog(Log);
					println(Log);
					
					// Remove message
					msgs.remove();
				}
			}else {
				now = LocalDateTime.now();
				now.minus(offset, ChronoUnit.MILLIS);
				String Log = dtf.format(now) +": Error: Only your heart can tell you what this In Message means";
				sendLog(Log);
				println(Log);
				
				// Remove message
				msgs.remove();
			}
		}
	}
	
	private void SendMsgToFloors(byte[] msg) {
		// Declare local variables
		byte[] newMsg = new byte[6];
		
		// Prepare notification
		if (msg[0] == 9) {
			newMsg = msg;
		}
		else if (msg[5] == 0) {
			newMsg[0] = 1;
			newMsg[1] = msg[2];
			newMsg[2] = msg[3];
			newMsg[3] = msg[4];
			newMsg[4] = msg[5];
			newMsg[5] = msg[6];
		} else if (msg[5] == 1) {
			newMsg[0] = 2;
			newMsg[1] = msg[2];
			newMsg[2] = msg[3];
			newMsg[3] = msg[4];
			newMsg[4] = msg[5];
			newMsg[5] = msg[6];
		}
		
		
		// Get lock on floor in messages
		synchronized(InMessagesFloors) {
			// Add message to in floor message queue
			InMessagesFloors.add(newMsg);
			
			// Notify all
			InMessagesFloors.notifyAll();
		}
	}
	
	private void InitFS(byte[] initMsg) {
		// Check if number of floors has already been set
		if (NumFloors != -1) {
			// 'Discontinue' existing floor threads
			for (int i = 0; i < FloorThreadObjs.length; i++) {
				FloorThreadObjs[i].discontinue();
			}
		}
		
		// Retrieve number of floors
		NumFloors = initMsg[2] * 10 + initMsg[3];
		
		// Initialize floor threads
		initializeThreads(NumFloors);
	}
	
	/*****************************************************************************
	 * Send confirmation message to scheduler
	 ****************************************************************************/
	private void sendConfirmationSched() {
		// Prepare confirmation message
		byte[] confirmationMsg = new byte[5];
		Arrays.fill(confirmationMsg, (byte)0);
		confirmationMsg[1] = (byte)1;
		confirmationMsg[3] = (byte)1;
		
		/**/ // Sending button press event
		now = LocalDateTime.now();
		now.minus(offset, ChronoUnit.MILLIS);
		String Log =dtf.format(now) + ":Floor:: Sending check in confirmation to Scheduler";
		sendLog(Log);
		println(Log);
//		printBMessage(confirmationMsg, confirmationMsg.length);
		/**/
		
		// Send confirmation message
		SendMessageToPort(confirmationMsg, 0);
	}
	/*****************************************************************************
	 * Write events to file
	 ****************************************************************************/
	private Thread[] initializeThreads(int NumFloors) {
		// Declare local variables
		Thread[] FloorThreads = new Thread[NumFloors];
		
		// Initialize floor thread objects array
		FloorThreadObjs = new FloorThread[NumFloors];
		
		// Initialize each thread
		for (int i = 0; i < NumFloors; i++) {
			// Initialize new floor thread object
			if (i == 0) {
				FloorThreadObjs[i] = new FloorThread(i + 1, 0, InMessagesFloors, OutMsgsFloors, o, offset, LoggerPort, LoggerIP);
			} else if (i == NumFloors - 1) {
				FloorThreadObjs[i] = new FloorThread(i + 1, 2, InMessagesFloors, OutMsgsFloors, o, offset, LoggerPort, LoggerIP);
			}else {
				FloorThreadObjs[i] = new FloorThread(i + 1, 1, InMessagesFloors, OutMsgsFloors, o, offset, LoggerPort, LoggerIP);
			}
			
			// Initialize new thread for the new floor thread
			FloorThreads[i] = new Thread(FloorThreadObjs[i]);
			
			// Start the new floor thread
			FloorThreads[i].start();
		}
		
		// Return array of floor threads
		return FloorThreads;
	}
	/*****************************************************************************
	 * Write events to file
	 ****************************************************************************/
	private void WriteEventsFile(FloorEvents[] FEvents, String fileName) {
		// Declare local variables
		Path dir = Paths.get("");
		String absolutePath = dir.toAbsolutePath().toString() + File.separator + fileName;
		
		// Write content to file
		try(FileWriter fwriter = new FileWriter(absolutePath)){
			// Write file headers
			fwriter.write("Time Stamp, Origin Floor, Direction, Destination Floor, Error \n");
			// Write floor events
			for (int i = 0; i < FEvents.length; i++) {
				fwriter.write(FEvents[i].time + "," + FEvents[i].origin + "," + FEvents[i].direction + "," + FEvents[i].destination + "," + FEvents[i].error +  "\n");
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	/*****************************************************************************
	 * Determine direction of movement
	 ****************************************************************************/
	private int getDir(int origin, int destination) {
		if (origin < destination) {
			return 1;
		}
		else {
			return 0;
		}
	}
	
	/*****************************************************************************
	 * Generate random integer within bounds
	 ****************************************************************************/
	private int GenRandInt(int low, int high) {
		// Declare local variables
		Random r = new Random();
		
		// Return random integer
		return r.nextInt(high-low) + low;
	}
	
	/*****************************************************************************
	 * Create floor event csv file
	 ****************************************************************************/
	private void createEventFile(int NumFloors, String FName) {
		// Declare local variables
		int TimeStamp = 0;
		int NumFatal = 2;
		int NumTransient = 3;
		int FatalCount = 0;
		int TransientCount = 0;
		
		// Iterate 1,000 times 
		for (int i = 0; i < NumEvents; i++) {
			// Do coin flip to determine if request originates at origin floor
			int CoinFlip = GenRandInt(0, 2);
			
			// Initialize floor event variable
			FEvents[i] = new FloorEvents();
			
			// If coin flip is 0 origin floor is ground floor
			if (CoinFlip == 0) {
				FEvents[i].origin = 1;
				FEvents[i].destination = FEvents[i].origin;
			}
			else {
				FEvents[i].origin = GenRandInt(1, NumFloors + 1);
				FEvents[i].destination = FEvents[i].origin;
			}
			
			// Choose destination floor not equal to origin floor
			while (FEvents[i].destination == FEvents[i].origin) {
				FEvents[i].destination = GenRandInt(1, NumFloors + 1);
			}
			
			// Choose time stamp
			TimeStamp = TimeStamp + GenRandInt(1000, 6000);
			FEvents[i].time = TimeStamp;
			
			// Get direction of request
			FEvents[i].direction = getDir(FEvents[i].origin, FEvents[i].destination);
			
			CoinFlip = GenRandInt(0, 2);
			if (TransientCount < NumTransient && CoinFlip == 0) {// Possibly make transient
				CoinFlip = GenRandInt(0, 2);
				if (CoinFlip == 0) {
					FEvents[i].error = 1;
					TransientCount++;
				}else {
					FEvents[i].error = 2;
				}
			}else if (FatalCount < NumFatal) { // Possibly make fatal
				CoinFlip = GenRandInt(0, 2);
				if (CoinFlip == 1) {
					FEvents[i].error = 0;
					FatalCount++;
				}else {
					FEvents[i].error = 2;
				}
			}
		}
		//****** For Testing
//		 FEvents = new FloorEvents[2];
//		 FEvents[0] = new FloorEvents();
//		 FEvents[1] = new FloorEvents();
//		 FEvents[0].origin = 2;
//		 FEvents[0].destination = 5;
//		 FEvents[0].direction = 1;
//		 FEvents[1].origin = 5;
//		 FEvents[1].destination = 1;
//		 FEvents[1].direction = 0;
		//***** For Testing 
		// Write events to event file
		WriteEventsFile(FEvents, FName);
	}
	
	/*****************************************************************************
	 * Read in floor event csv file
	 ****************************************************************************/
	private FloorEvents[] ReadEventsFile(int numFloors, String fName) {
		// Declare local variables
		Path dir = Paths.get("");
		String absolutePath = dir.toAbsolutePath().toString() + File.separator + fName;
		//FloorEvents[] FEvents = new FloorEvents[1000];
		int i = 0;
		
		// Read content to file
		try(BufferedReader bReader = new BufferedReader(new FileReader(absolutePath))){
			String line = bReader.readLine();
			line = bReader.readLine();
			while(line != null) {
				FEvents[i] = new FloorEvents();
				String[] lineSplit = line.split(",");
				FEvents[i].time = Integer.parseInt(lineSplit[0]);
				FEvents[i].origin = Integer.parseInt(lineSplit[1]);
				FEvents[i].direction = Integer.parseInt(lineSplit[2]);
				FEvents[i].destination = Integer.parseInt(lineSplit[3]);
				FEvents[i].error = Integer.parseInt(lineSplit[4]);
				i = i + 1;
				line = bReader.readLine();
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}catch (IOException e) {
			e.printStackTrace();
		}
		
		// Return Floor Events
		return FEvents;
	}
	
	/*****************************************************************************
	 * Print byte array message
	 ****************************************************************************/
	private void printBMessage(byte[] msg, int length) {
		// Print each byte individually 
		for (int i = 0; i < length; i++) {
			print(msg[i]);
		}
		
		// Add newline character
		print("\n");
	}
	
	/*****************************************************************************
	 * Used by threads to print to standard output
	 * 	- Remove IlleagalMonitorState error
	 ****************************************************************************/
	public void println(String x) {
	    // Print synchronized println
		synchronized (this) {
	        System.out.print(x + "\n");
	    }
	}
	public void print(String x) {
		// Print synchronized print for string input
		synchronized (this) {
	        System.out.print(x);
	    }
	}
	public void print(Byte x) {
		// Print synchronized print for byte input
	    synchronized (this) {
	        System.out.print(x);
	    }
	}
	
	/*****************************************************************************
	 * Returns boolean to indicate if it is time to dispatch the floor event
	 ****************************************************************************/
	private boolean isTimeForEvent(FloorEvents currFEvent) {
		// Store time difference
		long timeDiff = getTimeDiff();
		
		// Return whether or not time matches event time
		return timeDiff >= currFEvent.time;
	}
	
	/*****************************************************************************
	 * Get time difference since start
	 ****************************************************************************/
	private long getTimeDiff() {
		// Get current time
		Date currDate = new Date();
		long currTime = currDate.getTime();
		
		// Store time difference
		return currTime - startTime;
	}
	
	/*****************************************************************************
	 * Run method for main process thread
	 ****************************************************************************/
	public void run() {
		begin();
	}
}
