package FloorSimulator;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.util.concurrent.ConcurrentLinkedQueue;;

/*****************************************************************************
 * Class Functions
 * - Constructors
 * 	- Takes port, main process id and datagram new message object as inputs
 * - run
 * 	- Calls function to listen on port
 * - Listen
 * 	- Listens on port
 * 	- Calls Process Datagram function
 * - ProcessDatagram
 * 	- Processes the received datagram
 * 	- Sends message to main process
 * - println
 * 	- Prints to standard output from asynchronous context
 ****************************************************************************/
public class DataSocketSender implements Runnable{
	// Declare class attributes
	private int Port;
	private int ElevPort;
	private int SchedPort;
	private DatagramSocket Socket;
	private ConcurrentLinkedQueue<byte[]> msgs;
	private String SchedIP;
	private String ElevIP;
	
	/*****************************************************************************
	 * Constructor
	 * 	- Takes in:
	 * 		- Port: To listen for DatagramPackets on
	 * 		- Parent Process ID: To be able to notify main process of new messages
	 ****************************************************************************/
	public DataSocketSender(int newPort, int newSchedPort, int newElevPort, ConcurrentLinkedQueue<byte[]> newMsgs, String newSchedIP, String newElevIP) {
		// Initialize class attributes
		Port = newPort;
		ElevPort = newElevPort;
		SchedPort = newSchedPort;
		msgs = newMsgs;
		SchedIP = newSchedIP;
		ElevIP = newElevIP;
		try {
			Socket = new DatagramSocket(Port);
		} catch (SocketException e) {
			e.printStackTrace();
		}
	}
	
	/*****************************************************************************
	 * Run
	 * 	- Starts DataSocket Listener thread:
	 ****************************************************************************/
	public void run() {
		// Print start message
		println("DataSocketSender: DatagramSocket Sender has set it's sending port. Sending Port: " + Integer.toString(Port));
		
		while(true) {
			// Check if there are any outgoing messages
			GetOutMessages();
		}
	}
	
	/*****************************************************************************
	 * checkOutMsg
	 * 	- Checks if there is an out message to run
	 ****************************************************************************/
	private void GetOutMessages() {
		// Declare local variables
		ConcurrentLinkedQueue<byte[]> newMsgs = new ConcurrentLinkedQueue<byte[]>();
		
		// Check if there is a message
		synchronized(msgs) {
			while(msgs.isEmpty()) {
				try {
					msgs.wait();
				} catch (InterruptedException e) {
					// Print stack trace
					e.printStackTrace();
				}
			}
			
			// Check if there is a message
			if (msgs.isEmpty()) {
				return;
			}
			
			// Get all out messages
			while(!msgs.isEmpty()) {
				//**/ Print message received
//				print("DataSocketSender: ");
//				printBMessage(msgs.peek(), msgs.peek().length);
				newMsgs.add(msgs.remove());
			}
		}
		
		// Send messages
		SendMessages(newMsgs);
	}
	
	private void sortMessages(ConcurrentLinkedQueue<byte[]> SchedMsgs, ConcurrentLinkedQueue<byte[]> ElevMsgs, ConcurrentLinkedQueue<byte[]> msgs) {
		// Sort message Queue
		while(!msgs.isEmpty()) {
			// Take out message
			byte[] currMsg = msgs.remove();
			
			// Check if the message is for the scheduler
			if (currMsg[0] == (byte)0) {
				SchedMsgs.add(currMsg);
			} // Check if the message is for the elevator 
			else if (currMsg[0] == (byte)1) {
				ElevMsgs.add(currMsg);
			}
		}
	}
	
	private DatagramPacket CreateDatagramPacket(byte[] msg, int Port, String IP) {
		// Declare local variables
		byte[] data = new byte[msg.length-1];
		
		// Fill data 
		for (int i = 0; i < msg.length-1; i++) {
			data[i] = msg[i + 1];
		}
		
		// Return Datagram Packet
		try {
//			return new DatagramPacket(data, data.length, InetAddress.getLocalHost(), Port);
			return new DatagramPacket(data, data.length, InetAddress.getByName(IP), Port);
		} catch (UnknownHostException e) {
			e.printStackTrace();
		}
		
		// Return null if datagram packet was not sent
		return null;
	}
	
	private void SendDatagramPacket(DatagramPacket Packet) {
		// send the packet
		try {
			Socket.send(Packet);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	private void SendMessages(ConcurrentLinkedQueue<byte[]> msgs) {
		// Declare local variables
		ConcurrentLinkedQueue<byte[]> SchedMsgs = new ConcurrentLinkedQueue<byte[]>();
		ConcurrentLinkedQueue<byte[]> ElevMsgs = new ConcurrentLinkedQueue<byte[]>();
		
		// Sort the incoming messages
		sortMessages(SchedMsgs, ElevMsgs, msgs);
		
		// Iterate over each message for the elevator
		while (!ElevMsgs.isEmpty()) {
			// Declare variables 
			byte[] msg = ElevMsgs.remove();
			
			// Create Datagram packet
			 DatagramPacket Packet = CreateDatagramPacket(msg, ElevPort, ElevIP);
			 
			 // Send the packet
			 SendDatagramPacket(Packet);
		}
		
		// Iterate over each message for the elevator
		while (!SchedMsgs.isEmpty()) {
			// Declare variables 
			byte[] msg = SchedMsgs.remove();
			
			// Create Datagram packet
			 DatagramPacket Packet = CreateDatagramPacket(msg, SchedPort, SchedIP);
			 
			 // Send the packet
			 SendDatagramPacket(Packet);
		}
	}
	
	/*****************************************************************************
	 * Print byte array message
	 ****************************************************************************/
	private void printBMessage(byte[] msg, int length) {
		// Print each byte individually
		for (int i = 0; i < length; i++) {
			print(msg[i]);
		}
		// Print newline character
		print("\n");
	}
	
	/*****************************************************************************
	 * Used by threads to print to standard output
	 * 	- Remove IlleagalMonitorState error
	 ****************************************************************************/
	public void println(String x) {
		// Synchronized version of println for string
	    synchronized (this) {
	        System.out.print(x + "\n");
	    }
	}
	public void print(String x) {
		// Synchronized version of print for string
		synchronized (this) {
	        System.out.print(x);
	    }
	}
	public void print(Byte x) {
		// Synchronized version of print for byte
	    synchronized (this) {
	        System.out.print(x);
	    }
	}
}
