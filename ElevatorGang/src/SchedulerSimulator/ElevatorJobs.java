package SchedulerSimulator;

import java.util.Comparator;
import java.util.concurrent.PriorityBlockingQueue;

public class ElevatorJobs {
	// Class Attributes
	private PriorityBlockingQueue<Integer> downQueue;
	private PriorityBlockingQueue<Integer> upQueue;
	private long floorsDown = 0x000000;
	private long floorsUp = 0x000000;
	private long upFloorRequests = 0x000000;
	private long downFloorRequests = 0x000000;
	private int ElevDir = -1;
	private int CurrFloor = 1;
	private int State = 2;
	private boolean isActive;
	// Comparator for max heap
	Comparator<Integer> destComparator = new Comparator<Integer>() {
		@Override
		public int compare(Integer arg0, Integer arg1) {
			return - Integer.compare(arg0, arg1);
		}
	};
	
	// Constructor
	public ElevatorJobs(int numFloors) {
		downQueue = new PriorityBlockingQueue<Integer>();
		upQueue = new PriorityBlockingQueue<Integer>();
		this.isActive = true;
	}
	
	// Check if floor is already in queue
	private long checkFloorExists(int FloorNum, int Dir) {
		long bitmask = (byte)1 << FloorNum;
		
		if (Dir == 0) {
//			System.out.println("Check if floor exists");
//			System.out.println(bitmask & floorsDown);
			return bitmask & floorsDown;
		}else {
//			System.out.println("Check if floor exists");
//			System.out.println(bitmask & floorsUp);
			return bitmask & floorsUp;
		}
	}
	public PriorityBlockingQueue<Integer> getDownQueue() {
		return downQueue;
	}

	public void setDownQueue(PriorityBlockingQueue<Integer> downQueue) {
		this.downQueue = downQueue;
	}

	public PriorityBlockingQueue<Integer> getUpQueue() {
		return upQueue;
	}

	public void setUpQueue(PriorityBlockingQueue<Integer> upQueue) {
		this.upQueue = upQueue;
	}

	public long getUpFloorRequests() {
		return upFloorRequests;
	}

	public void setUpFloorRequests(long upFloorRequests) {
		this.upFloorRequests = upFloorRequests;
	}

	public long getDownFloorRequests() {
		return downFloorRequests;
	}

	public void setDownFloorRequests(long downFloorRequests) {
		this.downFloorRequests = downFloorRequests;
	}
	
	public boolean getIsActive() {
		return this.isActive;
	}
	
	public void setIsActive(boolean s) {
		this.isActive = s;
	}

	// Add a floor
	public boolean addFloor(int FloorNum, boolean typeVal, int Dir) {
//		int Dir = -1; 
//		if (FloorNum < CurrFloor) {
//			Dir = 0;
//		}else if (FloorNum > CurrFloor) {
//			Dir = 1;
//		}else {
//			return false;
//		}
		
		if ((Dir != 0 && Dir != 1) || FloorNum <= 0) {
			System.out.println("Direction for floor request is invalid or floor number is 0");
			return false;
		}
		else if (checkFloorExists(FloorNum, Dir) == 0) {
			if (Dir == 0) {
				if (typeVal) {
					downFloorRequests = downFloorRequests | ((byte)1 << FloorNum);
				}
				downQueue.add(FloorNum);
				floorsDown = floorsDown | ((byte)1 << FloorNum);
				changeDirIfNecess();
			} else if (Dir == 1) {
				if (typeVal) {
					upFloorRequests = upFloorRequests | ((byte)1 << FloorNum);
				}
				upQueue.add(FloorNum);
				floorsUp = floorsUp | ((byte)1 << FloorNum);
				changeDirIfNecess();
			}
			return true;
		}
		return false;
	}
	
	// Get next stop
	public int getNextStop() {
		switch (ElevDir) {
			case 0:
				if (downQueue.peek() == null) {
					return 0;
				}
				return downQueue.peek();
			case 1: 
				if (upQueue.peek() == null) {
					return 0;
				}
				return upQueue.peek();
		}
		
		return -1;
	}
	public void changeDirIfNecess() {
		if (downQueue.isEmpty() && !upQueue.isEmpty()) {
			State = 1;
			ElevDir = 1;
		}else if (!downQueue.isEmpty() && upQueue.isEmpty()) {
			State = 0;
			ElevDir = 0;
		} else if (downQueue.isEmpty() && upQueue.isEmpty()) {
			State = 2;
		}
	}
	
	// Remove next stop
	public boolean removeNextStop() {
		switch (ElevDir) {
			case 0: 
				downQueue.poll();
				changeDirIfNecess();
			case 1: 
				upQueue.poll();
				changeDirIfNecess();
		}
		
		return true;
	}
	
	// Set elevator direction
	public boolean goX(int Dir) {
		if (Dir == 0 || Dir == 1) {
			ElevDir = Dir;
			return true;
		}
		
		return false;
	}
	
	// Set current floor for elevator
	public void setCurrFloor(int newCurrFloor) {
		CurrFloor = newCurrFloor;
	}
	
	// Get current floor for elevator
	public int getCurrFloor() {
		return CurrFloor;
	}
	
	// Get direction
	public int getDir() {
		return ElevDir;
	}
	
	// Set direction
	public void setDir(int newDir) {
		ElevDir = newDir;
	}
	
	// Get state
	public int getState() {
		return State;
	}
	
	// Set state
	public void setState(int newState) {
		State = newState;
	}
	
	public void changeDir() {
		if (ElevDir == 0) {
			ElevDir = 1;
			State = 1;
		}else if (ElevDir == 1) {
			ElevDir = 0;
			State = 0;
		}
	}
	
	public void rmFReq(int FloorNum) {
		if (ElevDir == 0) {
			downFloorRequests = downFloorRequests - (downFloorRequests & ((byte)1 << FloorNum));
		}else if (ElevDir == 1) {
			upFloorRequests = upFloorRequests - (upFloorRequests & ((byte)1 << FloorNum));
		}
	}
}
