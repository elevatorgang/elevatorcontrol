package SchedulerSimulator;

import java.io.IOException;
import java.net.*;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.Arrays;
import java.util.TimerTask;

public class ElevatorTimerTask extends TimerTask{
	
	private DatagramPacket outMessage;
	private int elevNumber;
	private DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss.SSS");
	private LocalDateTime now;
	private static long offset;
	
	@Override
	public void run() {
		// TODO Auto-generated method stub
		DatagramSocket Out = null;
		try {
			Out = new DatagramSocket();
		} catch (SocketException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		byte[] msg = new byte[6];
		Arrays.fill(msg,(byte) 0);
		msg[0] = (byte) 3;
		msg[1] = (byte) elevNumber;
		msg[2] = (byte) 2;
		try {
			outMessage = new DatagramPacket(msg, msg.length, InetAddress.getLocalHost() , 1025);
		} catch (UnknownHostException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		now = LocalDateTime.now();
		now.minus(offset, ChronoUnit.MILLIS);
		System.out.println(dtf.format(now) + ":Scheduler:: Possible Fatal Error Detected in Elevator "+elevNumber);
		try {
			Out.send(outMessage);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Out.close();
	}
	
	public void setElevNumber(int e) {
		elevNumber = e;
	}

}
