package SchedulerSimulator;

import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.PriorityBlockingQueue;
/**
 * ElevatorJobQueue keeps track of the next stop the elevator 
 * @author kajhemmingsen
 *
 */
public class ElevatorJobQueue{

	// Test
	Comparator<Integer> destComparator = new Comparator<Integer>() {
		@Override
		public int compare(Integer arg0, Integer arg1) {
			return - Integer.compare(arg0, arg1);
		}
	};
	PriorityBlockingQueue<Integer> downQueue;
	PriorityBlockingQueue<Integer> upQueue;
	int floorsDown = 0x0000;
	int floorsUp = 0x0000;
	// Test end
	
	private List<Integer> activeQueue;
	private List<Integer> nextQueue;
	private List<Integer> upQueue1;
	private List<Integer> downQueue1;
	private List<Integer> upQueue2;
	private List<Integer> downQueue2;
	
	private int currentFloor;
	private int nextStop = 1;
	private int totalFloors;
	private int numStops = 0;
	
	private String direction;
	private boolean noJobs = true;
	
	public ElevatorJobQueue(int numFloors) { // Test
		this.currentFloor = 1;
		this.upQueue1 = new LinkedList<Integer>(); //list of ongoing stops while the elevator goes up
		this.downQueue1 = new LinkedList<Integer>(); //list of ongoing stops while the elevator goes down
		this.upQueue2 = new LinkedList<Integer>(); // list of stop requests wishing to go up after the elevator has passed the floor
		this.downQueue2 = new LinkedList<Integer>(); // list of stop requests wishing to go down after the elevator has passed the floor
		this.direction = "IDLE";
		this.activeQueue = new LinkedList<Integer>();
		
		// Test
		downQueue = new PriorityBlockingQueue<Integer>();
		upQueue = new PriorityBlockingQueue<Integer>(numFloors, destComparator);
		// Test end
	}
	
	// Test
	// Check if floor is already in queue
	public int checkFloorExists(int FloorNum, int Dir) {
		int bitmask = (byte)FloorNum;
		int test = 0x0004;
		
		if (Dir == 0) {
			System.out.println("Check if floor exists");
			System.out.println(bitmask & test);
			return bitmask & test;//floorsDown;
		}else {
			System.out.println("Check if floor exists");
			System.out.println(bitmask & floorsUp);
			return bitmask & floorsUp;
		}
	}
	public boolean addFloor(int FloorNum, int Dir) {
		if ((Dir != 0 && Dir != 1) || FloorNum <= 0) {
			System.out.println("Direction for floor request is invalid or floor number is 0");
			return false;
		}
		else if (checkFloorExists(FloorNum, Dir) == 0) {
			if (Dir == 0) {
				downQueue.add(FloorNum);
			} else if (Dir == 1) {
				upQueue.add(FloorNum);
			}
			System.out.println("Added floor " + Integer.toString(FloorNum));
		}
		return true;
	}
	// Test End
	
	public void setCurrentFloor(int currentFloor) {
		this.currentFloor = currentFloor;
	}
	
	public String getDirection() {
		return this.direction;
	}
	public int getCurrentFloor() {
		return this.currentFloor;
	}
	public List<Integer> getActiveQueue() {
		return this.activeQueue;
	}
	
	public List<Integer> getUpQueue1() {
		return this.upQueue1;
	}
	
	public List<Integer> getDownQueue1() {
		return this.downQueue1;
	}
	public void setTotalFloors(int totalFloors) {
		this.totalFloors = totalFloors;
	}
	
	public void setActiveQueue(List<Integer> activeQueue) {
		this.activeQueue = activeQueue;
		if(this.activeQueue.equals(upQueue1)) {
			this.direction = "UP";
		}
		else if(this.activeQueue.equals(downQueue1)) {
			this.direction = "DOWN";
		}
	}
	
	public void setNextStop() {
		if (numStops > 0) {
			nextStop = activeQueue.get(0);
		}
		else {
			nextStop = 0;
			direction = "IDLE";
			noJobs = true;
		}
	}
	
	public int getNextStop() {
		return nextStop;
	}
	public void addStop(int destinationFloor, String directionRequest, int currentFloor) {
		List<Integer> tempList = new LinkedList<Integer>();
		if (directionRequest.equals("UP")) {/**/
			if (upQueue1.isEmpty() && upQueue2.isEmpty()) {
				upQueue1.add(destinationFloor);
				if (noJobs) {
					setActiveQueue(upQueue1);
					noJobs = false;
				}
			}
			else if (upQueue1.size() < totalFloors && currentFloor < upQueue1.get(0) && !upQueue1.contains(destinationFloor)) {
				upQueue1.add(destinationFloor);
				tempList.addAll(sortList(directionRequest, upQueue1));
				upQueue1.removeAll(upQueue1);
				upQueue1.addAll(tempList);
				tempList.removeAll(tempList);

			}
			else if (!upQueue2.contains(destinationFloor)){
				upQueue2.add(destinationFloor);
				tempList.addAll(sortList(directionRequest, upQueue2));
				upQueue2.removeAll(upQueue2);
				upQueue2.addAll(tempList);
				tempList.removeAll(tempList);
			}
			else {
				System.out.println("Stop is already on the route");
			}
		}
		else if (directionRequest.equals("DOWN")) {/**/
			if (downQueue1.isEmpty() && downQueue2.isEmpty()) {
				downQueue1.add(destinationFloor);
				if (noJobs) {
					setActiveQueue(downQueue1);
					noJobs = false;
				}	
			}
			else if (downQueue1.size() < totalFloors && currentFloor > downQueue1.get(0) && !downQueue1.contains(destinationFloor)) {
				downQueue1.add(destinationFloor);
				tempList.addAll(sortList(directionRequest, downQueue1));
				downQueue1.removeAll(downQueue1);
				downQueue1.addAll(tempList);
				tempList.removeAll(tempList);
			}
			else if (!downQueue2.contains(destinationFloor)){
				downQueue2.add(destinationFloor);
				tempList.addAll(sortList(directionRequest, downQueue2));
				downQueue2.removeAll(downQueue2);
				downQueue2.addAll(tempList);
				tempList.removeAll(tempList);
			}
			else {
				System.out.println("Stop is already on the route");
			}	
		}
		setNextQueue(activeQueue);
		numStops++;
		setNextStop();
	}
	/**
	 * removes first entry in the active queue and the corresponding stop queue it mirrors
	 */
	public void removeStop() {
		if (!activeQueue.isEmpty()) {
			if (activeQueue.equals(upQueue1)) {
				upQueue1.remove(0);
				if (upQueue1.isEmpty()) {
					activeQueue.remove(0);
					upQueue1.addAll(upQueue2);
					upQueue2.removeAll(upQueue2);
					activeQueue = nextQueue;
					setNextQueue(activeQueue);
				}
				numStops--;
			}
			else if (activeQueue.equals(downQueue1)) {
				downQueue1.remove(0);
				if (downQueue1.isEmpty()) {
					activeQueue.remove(0);
					downQueue1.addAll(downQueue2);
					downQueue2.removeAll(downQueue2);
					activeQueue = nextQueue;
					setNextQueue(activeQueue);
					numStops--;
				}
			}
		}
		else {
			System.out.println("No jobs to remove");
		}
		setNextStop();
	}
	
	/**
	 * setNextQueue takes the current active linked list and checks that subsequent linked lists and sets them to follow in a round-robin fashion.
	 * @param currentList
	 */
	public void setNextQueue(List<Integer> currentList) {
		if (currentList.equals(upQueue1)) {
			if (!downQueue1.isEmpty()) {
				nextQueue = downQueue1;
			}
		}
		else if (currentList.equals(downQueue1)) {
			if (!upQueue1.isEmpty()) {
				nextQueue = upQueue1;
			}
		}
		else {
			System.out.println("No other pending queues");
		}
	}
	
	
	public List<Integer> sortList(String currentDirection, List<Integer> unsortedList) {
		if (currentDirection == "UP") {
			Collections.sort(unsortedList);
			return unsortedList;
		}
		else if (currentDirection == "DOWN") {
			Collections.sort(unsortedList,Collections.reverseOrder());
			return unsortedList;
		}
		else {
			System.out.println("List unchanged");
			return unsortedList;
		}
	}
}
