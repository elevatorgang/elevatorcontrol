package SchedulerSimulator;


import java.io.*;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Arrays;

import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;
import java.util.TreeMap;

import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.TimeUnit;

import javax.swing.JOptionPane;

import TimeSync.TimeSync;


/**
 * The main component of the Scheduler subsystem which handles 
 * @author kajhemmingsen
 *
 */
public class SchedulerSubsystem implements Runnable{
	/*****************************************************************************
	 * Class to hold times
	 *****************************************************************************/
	private class SchedTimes {
		// Declare class attributes
		public Instant ArriveTime;
		public long button = 0;
		public long floortimer = 0;
		public long floorbutton = 0;
		
		
		// Constructor
		SchedTimes(){
			
		}
	}
	
	/*****************************************************************************
	 * Public Variables
	 ****************************************************************************/
	//private ArrayList<ElevatorJobQueue> elevatorList;
	private TreeMap<Integer, Integer> upReqs = new TreeMap<Integer, Integer>();
	private TreeMap<Integer, Integer> downReqs = new TreeMap<Integer, Integer>();
	private ArrayList<ElevatorJobs> elevatorList;
	private boolean ElevInitialized = false;
	private boolean FloorInitialized = false;
	private boolean[] elevDest;
		
	private int numFloors;
	private int numElevators;
	
	//locks so requests aren't spammed
	private boolean floorResponded = false;
	private long timeFloorCheckInIssued = 0;
	private boolean elevatorResponded = false;
	private long timeElevatorCheckInIssued = 0;
	
	public LinkedBlockingQueue<byte[]> incomingQueue = new LinkedBlockingQueue<byte[]>();
	public LinkedBlockingQueue<byte[]> outgoingQueue = new LinkedBlockingQueue<byte[]>();
	public LinkedBlockingQueue<byte[]> pendingElevMessages = new LinkedBlockingQueue<byte[]>();
	
	private Timer[] timers;
	private ElevatorTimerTask[] tasks;

	private static String FloorIP = "172.17.32.255";
	private static String ElevIP = "172.17.32.255";
	private static String LoggerIP = "172.17.32.255";

	private static int LoggerPort = 1040;
	
	private DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss.SSS");
	private LocalDateTime now;
	

	private long startTime = 0;
	private long endTime = 0;
	private SchedTimes[] ST;
	
	private int Times = 0;
	
	private long offset;
	
	private void syncClock() {
		// NTP Clock Synchronization Test
		offset = TimeSync.getTimeOffset();
	}
	
	
	public SchedulerSubsystem(int numElevators, int numFloors) {
		this.numElevators = numElevators;
		this.numFloors = numFloors;
		elevatorList = new ArrayList<ElevatorJobs>(numElevators);
		for (int i = 0; i< numElevators; i++) {
			elevatorList.add(i,new ElevatorJobs(numFloors)); // Test Queue
		}
		timers = new Timer[numElevators];
		tasks = new ElevatorTimerTask[numElevators];
		elevDest = new boolean[numElevators];
		for(int i = 0 ; i < numElevators ; i++) {
			timers[i] = new Timer();
			tasks[i] = new ElevatorTimerTask();
			tasks[i].setElevNumber(i+1);
			elevDest[i]=false;
		}
		ST = new SchedTimes[1000];
		for (int i = 0 ; i < 1000 ; i++) {
			ST[i] = new SchedTimes();
		}
		syncClock();
	}
	
	// Find appropriate elevator
	private int findElev(int FloorNum, int Dir) {
		int minDist = numFloors + 1;
		int winner = -1;
		
		for (int i = 0; i < elevatorList.size(); i++) {
			int dist = elevatorList.get(i).getCurrFloor() - FloorNum;
			if (elevatorList.get(i).getIsActive() == true) {
				// Check direction of request
				if (elevatorList.get(i).getState() == 2) { // Elevator is idle
					elevatorList.get(i).setDir(Dir);
					elevatorList.get(i).setState(Dir);
					return i;
				}else if (Dir == 0) { // Going down
					// Check if request is in the elevator's path
					if (dist > 0) { // Request is below elevator's current floor
						// Check if this is a new minimum distance
						if (Math.abs(dist) < minDist) {
							minDist = Math.abs(dist);
							winner = i;
						}
					}
				}else if (Dir == 1) { // Going up
					// Check if request is in the elevator's path
					if (dist < 0) { // Request is above elevator's current floor
						// Check if this is a new minimum distance
						if (Math.abs(dist) < minDist) {
							minDist = Math.abs(dist);
							winner = i;
						}
					}
				}
			}
		}
		
		return winner;
	}
	
	private void sendLog(String Log) {
		now = LocalDateTime.now();
		now.minus(offset, ChronoUnit.MILLIS);
		byte[] log = Log.getBytes(StandardCharsets.UTF_8);
		DatagramSocket sendSocket;
		DatagramPacket sendPacket;
		try {
			
			//sendPacket = new DatagramPacket(msg, msg.length, InetAddress.getByName("172.17.40.149"), 1025);
			sendPacket = new DatagramPacket(log, log.length, InetAddress.getByName(LoggerIP), LoggerPort);
			sendSocket = new DatagramSocket(1042);
	         sendSocket.send(sendPacket);
	         sendSocket.close();
		} catch (UnknownHostException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	// Process Floor requests
	private void ProcessFloor(byte[] msg) {
		if (msg[0] == 0) { // init confirmation
			FloorInitialized = true;
			timeElevatorCheckInIssued = System.currentTimeMillis();
			timeFloorCheckInIssued = System.currentTimeMillis();
		}else if (msg[0] == 1) { // Check In Confirmation
			if (floorResponded == false) {
				timeFloorCheckInIssued =- System.currentTimeMillis();
				floorResponded = true;
			}
		}else if (msg[0] == 2) { // Floor Request
			if (FloorInitialized == false) {
				FloorInitialized = true;
			}
			int FloorNum = msg[1] * 10 + msg[2];
			int dir = msg[3];
			int Elev = findElev(FloorNum, msg[3]);
			now = LocalDateTime.now();
			now.minus(offset, ChronoUnit.MILLIS);
			String Log = dtf.format(now) + ":Scheduler:: Scheduler:: Floor Request for Floor " + FloorNum + " going ";
			if (msg[3] == 0) {
				Log += "up received from floor subsystem.";
			}else if (msg[3] == 1) {
				Log += "down received from floor subsystem.";
			}
			if (Elev == -1) {
				if (msg[3] == 0) {
					Log += " No elevator to assign request to at the moment. Storing the request in up queue";
					upReqs.put(FloorNum, FloorNum);
				}else if (msg[3] == 1) {
					downReqs.put(FloorNum, FloorNum);
					Log += " No elevator to assign request to at the moment. Storing the request in up queue";
				}
			}else {
				Log += " Assigned to elevator " + (Elev + 1) + ".  ";
				int oldDest = elevatorList.get(Elev).getNextStop();
				elevatorList.get(Elev).addFloor(FloorNum,false,dir); // , msg[3]
				if (oldDest != elevatorList.get(Elev).getNextStop()) {
					Log += " Will be the elevator's next stop.";
					elevatorList.get(Elev).rmFReq(FloorNum);
					byte[] outMsg = new byte[6];
					byte[] bitmask = new byte[6];
					Arrays.fill(outMsg, (byte)0);
					Arrays.fill(bitmask, (byte)0);
					bitmask[4] = (byte)Elev;
					outMsg[0] = (byte)1;
					outMsg[1] = (byte)(Elev + 1);
					outMsg[2] = (byte)((FloorNum - FloorNum%10)/10);
					outMsg[3] = (byte)(FloorNum % 10);
					pendingElevMessages.add(bitmask);
					now = LocalDateTime.now();
					now.minus(offset, ChronoUnit.MILLIS);
					String Log2 = dtf.format(now) + ":Scheduler:: Sending a new stop at floor " + FloorNum + " for Elevator " + (Elev+1) + " to the elevator subsystem";
					sendLog(Log2);
					System.out.println(Log2);
					outMessageAdd(outMsg);
				}else {
					Log += " But, will not be the elevator's next stop";
				}
				sendLog(Log);
				System.out.println(Log);
			}
			endTime = System.nanoTime();
			ST[Times].floorbutton = endTime - startTime;
			
			Times++;
			endTime = 0;
			startTime = 0;
			printOutFile();
		}
	}
	
	// Process Elevator Requests
	private void ProcessElevators(byte[] msg) {
		if (msg[0] == 0 && msg[4] != 0) { // Elevator Confirmation
			if(pendingElevMessages.contains(msg)) {
				now = LocalDateTime.now();
				now.minus(offset, ChronoUnit.MILLIS);
				String Log = dtf.format(now)+ ":Scheduler:: Confirmation received for elevator: " + msg[4];
				sendLog(Log);
				System.out.println(Log);
				pendingElevMessages.remove(msg);
			}
		}else if (msg[0] == 0 && msg[4] == 0) { // Check In Confirmation
			if (elevatorResponded == false) {
				timeElevatorCheckInIssued = System.currentTimeMillis();
				elevatorResponded = true;
			}
		}else if (msg[0] == 1) { // Status update
			int ElevNum = msg[1] - 1;
			int FloorNum = msg[4] * 10 + msg[5];
			
			elevatorList.get(ElevNum).setCurrFloor(FloorNum);
			
			if (msg[3] == 0) {// Elevator has arrived at destination
				timers[ElevNum].cancel();
				timers[ElevNum] = new Timer();
				tasks[ElevNum] = new ElevatorTimerTask();
				tasks[ElevNum].setElevNumber(ElevNum+1);
				
				setNextStop(ElevNum);
				
			} else {
				timers[ElevNum].cancel();
				timers[ElevNum] = new Timer();
				tasks[ElevNum] = new ElevatorTimerTask();
				tasks[ElevNum].setElevNumber(ElevNum+1);
				timers[ElevNum].schedule(tasks[ElevNum], 15000);
			}
			endTime = System.nanoTime();
			ST[Times].floortimer = endTime - startTime;
			
			Times++;
			endTime = 0;
			startTime = 0;
			printOutFile();
			
		}else if (msg[0] == 2) { // Button press
			int ElevNum = msg[1]-1;
			int FloorNum = msg[2] * 10 + msg[3];
			int Dir;
			
			int oldDest = elevatorList.get(ElevNum).getNextStop();
			if(FloorNum < elevatorList.get(ElevNum).getCurrFloor()) {
				Dir = 0;
			}else {
				Dir = 1;
			}
			elevatorList.get(ElevNum).addFloor(FloorNum, true,Dir); // , msg[3]
			
			if (oldDest != elevatorList.get(ElevNum).getNextStop()) {
				byte[] outMsg = new byte[6];
				byte[] bitmask = new byte[6];
				Arrays.fill(outMsg, (byte)0);
				Arrays.fill(bitmask, (byte)0);
				bitmask[4] = (byte)ElevNum;
				outMsg[0] = (byte)1;
				outMsg[1] = (byte)(ElevNum + 1);
				outMsg[2] = (byte)((FloorNum - FloorNum%10)/10);
				outMsg[3] = (byte)(FloorNum % 10);
				pendingElevMessages.add(bitmask);
				now = LocalDateTime.now();
				now.minus(offset, ChronoUnit.MILLIS);
				String Log = dtf.format(now) + ":Scheduler:: Sending button press stop confirmation for floor " + FloorNum + " for Elevator " + (ElevNum+1) + " to the elevator subsystem";
				sendLog(Log);
				System.out.println(Log);
				outMessageAdd(outMsg);
			}
			
			endTime = System.nanoTime();
			ST[Times].button = endTime - startTime;
			
			Times++;
			endTime = 0;
			startTime = 0;
			printOutFile();
		}else if (msg[0] == 3) { // Elevator error
			int Elev = msg[1]-1;
			int ErrType = msg[2];
			now = LocalDateTime.now();
			now.minus(offset, ChronoUnit.MILLIS);
			String Log;
			if (ErrType == 0) {
				Log = dtf.format(now) +":Scheduler:: Elevator resolved error detected on elevator " + (Elev+1);
				elevatorList.get(Elev).setIsActive(true);		
			} else if (ErrType == 1) {
				Log = dtf.format(now) + ":Scheduler:: Elevator transient error detected on elevator " + (Elev+1);
				elevatorList.get(Elev).setIsActive(false);
				rm(elevatorList.get(Elev), Elev, false);
			} else if (ErrType == 2) {
				Log = dtf.format(now) + ":Scheduler:: Elevator hard error message received for elevator " + (Elev+1);
				elevatorList.get(Elev).setIsActive(false);
				rm(elevatorList.get(Elev), Elev, true);
			}else {
				return;
			}
			sendLog(Log);
			System.out.println(Log);
		}else if (msg[0] == 9) { // Elevator init confirmation
			ElevInitialized = true;
			
			initFloors();
		}
	}
	//handles all confirmation messages
	public void confirmationSend() {
		sendElevatorConfirmation();
		sendFloorConfirmation();
	}
	
	private void confirmationCheck() {
		floorTimedOut();
		elevatorTimedOut();
	}
	
	public void sendElevatorConfirmation() {
		elevatorResponded = false;
		byte[] confMsg = new byte[6];
		Arrays.fill(confMsg, (byte)0);
		outgoingQueue.add(confMsg);
		timeElevatorCheckInIssued = System.currentTimeMillis();
		now = LocalDateTime.now();
		now.minus(offset, ChronoUnit.MILLIS);
		String Log = dtf.format(now) + ":Scheduler:: Elevator confirmation message sent.";
		sendLog(Log);
		System.out.println(Log);
	}
	
	public void sendFloorConfirmation() {
		floorResponded = false;
		byte[] confMsg = new byte[4];
		Arrays.fill(confMsg, (byte)0);
		confMsg[0] = (byte)1;
		outgoingQueue.add(confMsg);
		timeElevatorCheckInIssued = System.currentTimeMillis();
		now = LocalDateTime.now();
		now.minus(offset, ChronoUnit.MILLIS);
		String Log = dtf.format(now) +":Scheduler:: Floor confirmation message sent.";
		sendLog(Log);
		System.out.println(Log);
	}
	//check to see if the floor timed out
	public void floorTimedOut() {
		long currentTime = System.currentTimeMillis();
		if (((currentTime - timeFloorCheckInIssued) > 10000) && floorResponded == false && timeFloorCheckInIssued != 0) {
			now = LocalDateTime.now();
			now.minus(offset, ChronoUnit.MILLIS);
			String Log = dtf.format(now) +":Scheduler:: Floor subystem timed out";
			sendLog(Log);
			System.out.println(Log);
		}
	}
	//check to see if the elevator timed out
	public void elevatorTimedOut() {
		long currentTime = System.currentTimeMillis();
		if (((currentTime - timeElevatorCheckInIssued) > 10000) && elevatorResponded == false && timeElevatorCheckInIssued != 0) {
			now = LocalDateTime.now();
			now.minus(offset, ChronoUnit.MILLIS);
			String Log = dtf.format(now) + ":Scheduler:: Elevator subsytem timed out";
			sendLog(Log);
			System.out.println(Log);
		}
	}
	
	private void sendNextstop(int Elev, int FloorNum) {
		byte[] outMsg = new byte[6];
		byte[] bitmask = new byte[6];
		Arrays.fill(outMsg, (byte)0);
		Arrays.fill(bitmask, (byte)0);
		bitmask[4] = (byte)Elev;
		outMsg[0] = (byte)1;
		outMsg[1] = (byte)(Elev + 1);
		outMsg[2] = (byte)((FloorNum - FloorNum%10)/10);
		outMsg[3] = (byte)(FloorNum % 10);
		pendingElevMessages.add(bitmask);
		now = LocalDateTime.now();
		now.minus(offset, ChronoUnit.MILLIS);
		String Log = dtf.format(now) + ":Scheduler:: Sending a new stop at floor " + FloorNum + " for elevator " + (Elev + 1) + " to the elevator subsystem";
		sendLog(Log);
		System.out.println(Log);
		outMessageAdd(outMsg);
	}
	
	private void addStops(int ElevNum, int StartFloor, int Dir) {
		elevatorList.get(ElevNum).setDir(Dir);
		
		if (Dir == 0) {
			for (Map.Entry<Integer, Integer> entry: downReqs.entrySet()) {
				if (entry.getValue() <= StartFloor) {
					elevatorList.get(ElevNum).addFloor(entry.getValue(),false,Dir);
				}
			}
		}else if (Dir == 1) {
			for (Map.Entry<Integer, Integer> entry: upReqs.entrySet()) {
				if (entry.getValue() >= StartFloor) {
					elevatorList.get(ElevNum).addFloor(entry.getValue(),false,Dir);
				}
			}
		}
		
		sendNextstop(ElevNum, elevatorList.get(ElevNum).getNextStop());
	}
	
	private void getDistFromUnassigned(int FloorNum, int ElevNum) {
		int Upmin = -1;
		int Upwinner = -1;
		int Downmin = -1;
		int Downwinner = -1;
		
		for (Map.Entry<Integer, Integer> entry: upReqs.entrySet()) {
			int dist = Math.abs(entry.getValue() - FloorNum);
			if (dist < Upmin || Upmin == -1) {
				Upmin = dist;
				Upwinner = entry.getValue();
			}
		}
		
		for (Map.Entry<Integer, Integer> entry: downReqs.entrySet()) {
			int dist = Math.abs(entry.getValue() - FloorNum);
			if (dist < Downmin || Downmin == -1) {
				Downmin = dist;
				Downwinner = entry.getValue();
			}
		}
		
		if (Upmin < Downmin) {
			addStops(ElevNum, Upwinner, 1);
		}else if (Downwinner != -1){
			addStops(ElevNum, Downwinner, 0);
		}
	}
	// Set next elevator stop
	private void setNextStop(int ElevNum) {
		// Remove current destination
		if (elevatorList.get(ElevNum).getCurrFloor() == elevatorList.get(ElevNum).getNextStop() && elevatorList.get(ElevNum).getIsActive()) {
			now = LocalDateTime.now();
			now.minus(offset, ChronoUnit.MILLIS);
			String Log = dtf.format(now) + ":Scheduler:: Removed stop " + elevatorList.get(ElevNum).getNextStop() + " from elevator " + (ElevNum + 1) + ".  ";
			sendLog(Log);
			System.out.println(Log);
			elevatorList.get(ElevNum).removeNextStop();
			elevatorList.get(ElevNum).changeDirIfNecess();
			if (elevatorList.get(ElevNum).getNextStop() == 0 && elevatorList.get(ElevNum).getState() != 2) {
				elevatorList.get(ElevNum).setState(2);
				now = LocalDateTime.now();
				now.minus(offset, ChronoUnit.MILLIS);
				
				Log = dtf.format(now) + ":Scheduler:: No more stops for elevator " + (ElevNum + 1) + ". Sending to ground floor.";
				elevatorList.get(ElevNum).setDir(0);
				elevatorList.get(ElevNum).addFloor(1, false, 0);
				sendLog(Log);
				System.out.println(Log);
				sendNextstop(ElevNum,elevatorList.get(ElevNum).getNextStop());
				return;
			}
		}
		
		// Check if elevator has no next stop
		if (elevatorList.get(ElevNum).getNextStop() == 0) {
			elevatorList.get(ElevNum).changeDirIfNecess();
			if (elevatorList.get(ElevNum).getNextStop() == 0) {
				getDistFromUnassigned(elevatorList.get(ElevNum).getCurrFloor(),ElevNum);
			}else {// Elevator has switched directions
				sendNextstop(ElevNum, elevatorList.get(ElevNum).getNextStop());
			}
		}else {
			sendNextstop(ElevNum, elevatorList.get(ElevNum).getNextStop());
		}
	}
	
	// Add entries to elevator
	private void addEntries(TreeMap<Integer, Integer> Reqs, int ElevNum, int currFloor, int Dir) {
		for (Map.Entry<Integer, Integer> entry: Reqs.entrySet()) {
			if (Dir == 0 && entry.getValue() < currFloor) {
				elevatorList.get(ElevNum).addFloor(entry.getValue(), false,Dir);
			}else if (Dir == 1 && entry.getValue() > currFloor) {
				elevatorList.get(ElevNum).addFloor(entry.getValue(), false,Dir);
			}
		}
	}
	/**
	 * Adds a byte array to the outgoing queue, handling one input to be sent at a time
	 * @param data
	 */
	public void outMessageAdd(byte[] data) {
		outgoingQueue.add(data);
	}
	
	/**
	 * Peeks at the first item in the incoming queue, moves it to a local queue to be modified
	 * @return
	 */
	private LinkedBlockingQueue<byte[]> messageGet() {
		LinkedBlockingQueue<byte[]> messages = new LinkedBlockingQueue<byte[]>();

		byte[] msg = null;
		try {
			msg = incomingQueue.poll(1000, TimeUnit.MILLISECONDS);
		} catch (InterruptedException e) {
			confirmationCheck();
		}
		
		if (msg != null) {
			messages.add(msg);
		}
		
		return messages;
	}

	public void printByteMsg(byte[] msg) {
		System.out.print("Message is : ");
		for (int i = 0; i < msg.length; i++) {
			System.out.print(msg[i]);
		}
	}
	
	private void printOutFile() {
		// Declare local variables
				Path dir = Paths.get("");
				String absolutePath = dir.toAbsolutePath().toString() + File.separator + "OutputFile.csv";
				
				// Write content to file
				try(FileWriter fwriter = new FileWriter(absolutePath)){
					// Write file headers
					fwriter.write("Received Time, Elevator Button Response Time, Floor Sensor Response Time, Floor Event Response Time\n");
					// Write floor events
					for (int i = 0; i < ST.length; i++) {
						fwriter.write(ST[i].ArriveTime + "," + ST[i].button + "," + ST[i].floortimer + "," + ST[i].floorbutton  + "\n");
					}
				} catch (IOException e) {
					e.printStackTrace();
				}
    }
	
	public void processMessages() {
		LinkedBlockingQueue<byte[]> messages;

		// Retrieve messages from data socket listener message queue
		messages = messageGet();
		
		if (messages == null) {
			return;
		}
		startTime = System.nanoTime();
		ST[Times].ArriveTime = Instant.now();
		while(!messages.isEmpty()) {
			
			if (messages.peek().length == 6) {
				ProcessElevators(messages.remove());
			}else if (messages.peek().length == 4) {
				ProcessFloor(messages.remove());
			}
		}
	}
	
	private int getDirMessage(byte[] msg, ElevatorJobs Elev) {
		int ReqOrigin = msg[1] * 10 + msg[2];
		int ElevOrigin = Elev.getCurrFloor();
		
		return ReqOrigin - ElevOrigin;
	}
	
	private int findApprElev(byte[] msg, ArrayList<ElevatorJobs> Elevs) {
		int min = numFloors + 1;
		int minInd = -1;
		int currDist = 0;
		for (int i = 0; i < Elevs.size(); i++) {
			currDist = getDirMessage(msg, Elevs.get(i));
			if (currDist <= min && Elevs.get(i).getIsActive()) {
				min = currDist;
				minInd = i;
			}
		}
		return minInd;
	}

	/**
	 * Sends messages with corresponding codes to outgoing message queue
	 */
	public void initElevs() {
		byte[] elevatorInitCode = new byte[6];
		
		//setup how many elevators
		elevatorInitCode[0] = (byte)((numElevators - numElevators%10)/10);
		elevatorInitCode[1] = (byte)(numElevators%10);
		elevatorInitCode[2] = (byte)((numFloors - numFloors%10)/10);
		elevatorInitCode[3] = (byte)(numFloors%10);
		elevatorInitCode[4] = (byte)0;
		elevatorInitCode[5] = (byte)0;
		now = LocalDateTime.now();
		now.minus(offset, ChronoUnit.MILLIS);
		String Log = dtf.format(now) +":Scheduler:: Sending initialization message for " + numElevators + " elevators with " + numFloors + " floors";
		sendLog(Log);
		System.out.println(Log);
		outMessageAdd(elevatorInitCode);
	}
	
	public void rm(ElevatorJobs ej, int elev, boolean isFatal) {
		long elevsChanged = 0x000000;
		ej.setIsActive(false);
		byte[] newStopSend = new byte[3];
		newStopSend[0] = (byte)0;
		while(!ej.getUpQueue().isEmpty()) {
			int upReq = ej.getUpQueue().remove();
			now = LocalDateTime.now();
			now.minus(offset, ChronoUnit.MILLIS);
			String Log = dtf.format(now) + ":Scheduler:: Removed stop " + upReq + " from elevator " + (elev + 1) + ".  ";
			sendLog(Log);
			System.out.println(Log);
			if (((byte)1 << upReq & ej.getUpFloorRequests()) != 0 || isFatal == false) {
				newStopSend[1] = (byte)(upReq/10);
				newStopSend[2] = (byte)(upReq%10);
				int Elev = findApprElev(newStopSend, elevatorList);
				now = LocalDateTime.now();
				now.minus(offset, ChronoUnit.MILLIS);
				if (Elev != -1) {
					elevatorList.get(Elev).addFloor(upReq, false, 1);
					elevsChanged = elevsChanged | ((byte)1 << Elev);
					Log = dtf.format(now) +":Scheduler:: Reassigned up floor request on floor " + upReq + " to elevator " + (Elev+1);
				}else {
					upReqs.put(upReq, upReq);
					Log = dtf.format(now) + ":Scheduler:: Button press on elevator " + (Elev+1) + " to go up to floor " + upReq + " added to unassigned requests as no elevator is in its current path";
				}
				sendLog(Log);
				System.out.println(Log);
			}
			else {
				now = LocalDateTime.now();
				now.minus(offset, ChronoUnit.MILLIS);
				Log = dtf.format(now) + ":Scheduler:: Button push requestto floor "+upReq+" on elevator "+(elev+1) + " removed as passenger is stuck";
				sendLog(Log);
				System.out.println(Log);
			}
		}
		while(!ej.getDownQueue().isEmpty()) {
			int downReq = ej.getDownQueue().remove();
			now = LocalDateTime.now();
			now.minus(offset, ChronoUnit.MILLIS);
			String Log = dtf.format(now) + ":Scheduler:: Removed stop " + downReq + " from elevator " + (elev + 1) + ".  ";
			sendLog(Log);
			System.out.println(Log);
			if (((byte)1 << downReq & ej.getDownFloorRequests()) != 0 || isFatal == false) {
				newStopSend[1] = (byte)(downReq/10);
				newStopSend[2] = (byte)(downReq%10);
				int Elev = findApprElev(newStopSend, elevatorList);
				now = LocalDateTime.now();
				now.minus(offset, ChronoUnit.MILLIS);
				if (Elev != -1) {
					elevatorList.get(Elev).addFloor(downReq, false, 0);
					elevsChanged = elevsChanged | ((byte)1 << Elev);
					Log = dtf.format(now) + ":Scheduler:: Reassigned down floor request on floor " + downReq + " to elevator " + (Elev+1);
				}else {
					upReqs.put(downReq, downReq);
					Log = dtf.format(now) +":Scheduler:: Button press on elevator " + (Elev+1) + " to go down to floor " + downReq + " added to unassigned requests as no elevator is in its current path";
				}
				sendLog(Log);
				System.out.println(Log);
			}
			else {
				now = LocalDateTime.now();
				now.minus(offset, ChronoUnit.MILLIS);
				Log = dtf.format(now) + ":Scheduler:: Button push request to floor "+downReq+" on elevator "+(elev+1)+" removed as passenger is stuck";
				sendLog(Log);
				System.out.println(Log);
			}
		}
		ej.setDownFloorRequests(ej.getDownFloorRequests() & 0);
		ej.setUpFloorRequests(ej.getUpFloorRequests() & 0);
		
		updateElevNextStops(elevsChanged);
	}
	
	private void updateElevNextStops(long elevsChanged) {
		for(int i = 0; i < elevatorList.size();i++) {
			if (elevatorList.get(i).getIsActive() == true && (elevsChanged & ((byte)1<<i)) != 0) {
				sendNextstop(i,elevatorList.get(i).getNextStop());
			}
		}
	}
	public void initFloors() {
		byte[] floorInitCode = new byte[4];
		
		//setup how many floors
		floorInitCode[0] = (byte)0;
		floorInitCode[1] = (byte)((numFloors - numFloors%10)/10);
		floorInitCode[2] = (byte)(numFloors%10);
		floorInitCode[3] = (byte)0;
		now = LocalDateTime.now();
		now.minus(offset, ChronoUnit.MILLIS);
		String Log = dtf.format(now) +":Scheduler:: Sending " + numFloors + " floors to be initialized by the Floor Subsystem";
		outMessageAdd(floorInitCode);
		sendLog(Log);
		System.out.println(Log);
	}
	
	public static void main(String[] args) {
		String test1= JOptionPane.showInputDialog("Please input the desired number of elevators (1-7): ");
		int elevs = 0;
		try{
			elevs = Integer.parseInt(test1);
		} catch(NumberFormatException e) {
			elevs = 0;
		}
		while (elevs < 1 || elevs > 7) {
			test1= JOptionPane.showInputDialog("Invalid input.  Please input the desired number of elevators (1-12): ");
			try{
				elevs = Integer.parseInt(test1);
			} catch(NumberFormatException e) {
				elevs = 0;
			}
		}
		String test2= JOptionPane.showInputDialog("Please input the desired number of floors (1-100): ");
		int floors = 0;
		try{
			floors = Integer.parseInt(test2);
		} catch(NumberFormatException e) {
			floors = 0;
		}
		while (floors < 1 || elevs > 12) {
			test2= JOptionPane.showInputDialog("Invalid input.  Please input the desired number of floors (1-100): ");
			try{
				floors = Integer.parseInt(test2);
			} catch(NumberFormatException e) {
				floors = 0;
			}
		}
		
		SchedulerSubsystem ss = new SchedulerSubsystem(elevs,floors);
		//initialize all socket threads
		Thread floorSocketListener = new Thread(new SocketListener(1033, ss, 4));
		Thread elevSocketListener = new Thread(new SocketListener(1025, ss, 6));
		Thread socketSender = new Thread(new SocketSender(ss, FloorIP, ElevIP));
		Thread schedulerSubsystem = new Thread(ss);
		
		
		
		//start all extra threads
		elevSocketListener.start();
		floorSocketListener.start();
		socketSender.start();
		schedulerSubsystem.start();	
	}
	
	@Override
	public void run() {
		
		
		
		// Initialize elevators
		initElevs();
		while (true) {
			processMessages();
			
		}
	}

	
}
