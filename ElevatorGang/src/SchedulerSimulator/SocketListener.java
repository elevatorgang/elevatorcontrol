package SchedulerSimulator;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.SocketException;

/**
 * Listens for inputs on a designated port then relays the message to the main subsystem
 * @author kajhemmingsen
 *
 */
public class SocketListener implements Runnable{

	private DatagramSocket receiveSocket;
	private DatagramPacket receivePacket;
	private int msgLen;
	private SchedulerSubsystem ss;
	
	
	public SocketListener(int portID, SchedulerSubsystem ss, int newMsgLen) {
		this.ss = ss;
		msgLen = newMsgLen;
		try {
			receiveSocket = new DatagramSocket(portID);
		} catch (SocketException e) {
			e.printStackTrace();
		}
	}
	
	@Override
	public void run() {
		while(true) {
			byte[] msgBuf = new byte[msgLen];
			receivePacket = new DatagramPacket(msgBuf, msgBuf.length);
			try {
				receiveSocket.receive(receivePacket);
			} catch (IOException e) {
				e.printStackTrace();
			}
			byte[] msg = receivePacket.getData();
			/*
			System.out.print("Received message containing: ");
			for (int i = 0; i < receivePacket.getData().length; i++) {
				System.out.print(receivePacket.getData()[i]);
			}
			System.out.println();*/
			ss.incomingQueue.add(msg);
		}
		
	}

	
}
