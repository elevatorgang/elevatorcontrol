package SchedulerSimulator;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;
import java.net.UnknownHostException;

public class SocketSender implements Runnable {

	DatagramSocket socketSender;
	DatagramPacket sendPacket;
	int port;
	private SchedulerSubsystem ss;
	private String FloorIP;
	private String ElevIP;
	
	public SocketSender(SchedulerSubsystem ss, String newFloorIP, String newElevIP) {
		FloorIP = newFloorIP;
		ElevIP = newElevIP;
		try {
			socketSender = new DatagramSocket();
		} catch (SocketException se) {
			se.printStackTrace();
		}
		this.ss = ss;

	}
	@Override
	public void run() {
		while(true) {
			byte[] data;
			try {
				data = ss.outgoingQueue.take();
			} catch (InterruptedException e1) {
				e1.printStackTrace();
				continue;
			}
			
			// Set destination port
			String IP = "";
			if (data.length == 4) {
				port = 1026;
				IP = FloorIP;
			}
			else if (data.length == 6) {
				port = 1030;
				IP = ElevIP;
			}
			
			// Create send packet
			try {
//				sendPacket = new DatagramPacket(data,data.length,InetAddress.getLocalHost(),port);
				sendPacket = new DatagramPacket(data,data.length,InetAddress.getByName(IP),port);
			} catch (UnknownHostException e) {
				e.printStackTrace();
			}
			
			// Send the packet 
			try {
				socketSender.send(sendPacket);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

}
