package MasterSimulator;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.concurrent.TimeUnit;

import ElevatorSimulator.ElevatorSubsystem;
import FloorSimulator.FloorSubsystem;
import Logger.Logger;
//System controller, receives updates from the subsystems on any changes that would need to be reflected in the GUI
// Wello
public class Overseer { 
	private int elevators; //Number of elevators
	private int storys; //Number of floors
	private ElevatorSubsystem es; //Elevator subsystem
	private FloorSubsystem fs; //Floor subsystem

	private LocalDateTime now; //Variable for taking the timestamp for events
	private String FloorIP = "172.17.32.255";
	private String SchedIP = "172.17.32.255";
	private String ElevIP = "172.17.32.255";
	private String LoggerIP = "172.17.32.255";

	private int LoggerPort = 1040;
	//Holder class for info on an elevator's state
	private class ElevState {

		// Declare class attributes
		public int dest = 0;
		public int floor = 1;
		public int state = 0;
		public boolean door = false;
		public boolean motor = false;
		public boolean lamps[];
		public boolean inError = false;
		
		
		// Constructor
		ElevState(int floor){
			lamps = new boolean[floor];
			for (int i = 0 ; i < floor ; i++) {
				lamps[i] = false;
				
			}
		}
	}

	//Holder class for info on a floor's state
	private class FloorState{
		public boolean up;
		public boolean down;
	}	
	
	private ElevState elevs[]; //Array holding the ElevState objects for the system
	private FloorState floors[]; //Array holding the FloorState objects for the system
	private ArrayList<Event> Events; //ArrayList for holding the events recorded by Overseer, ArrayList used for dynamic length
	
	private SystemView view; //Class for generating and updating the GUI during the system runtime
	
	
	public Overseer() { //Constructor.  Sets up runs the subsystem threads
		Logger logger = new Logger(LoggerPort);
		Thread logThr = new Thread(logger);
		logThr.start();
		es = new ElevatorSubsystem(this, FloorIP, SchedIP, LoggerIP, LoggerPort);
		fs = new FloorSubsystem(this, ElevIP, SchedIP, LoggerIP, LoggerPort);
		Thread elevprocess = new Thread(es);
		Thread floorprocess = new Thread(fs);
		Events = new ArrayList<Event>();
		elevprocess.start();
		floorprocess.start();
		view = new SystemView();
		
	}
	
	//Method for receiving the initialization data Elevator receives from Scheduler
	public void Init(int e, int f) {
		elevators = e;
		storys = f;
		elevs = new ElevState[e];
		for(int i = 0 ; i < e ; i++) {
			elevs[i] = new ElevState(f);
		}
		floors = new FloorState[f];
		for(int i = 0 ; i < f ; i++) {
			floors[i] = new FloorState();
		}
		view.initialize(elevators, storys);
	}
	
	//Updates the state and current floor data for a specified elevator
	public void elevUpdate(int num, int state, int floor) {
		
		elevs[num-1].floor=floor;
		elevs[num-1].state=state;
		now = LocalDateTime.now();
		Event e = new Event(now, "Elevator "+num+" is state "+state+" (Stationary/Ascending/Dexcending) on Floor "+floor);
		Events.add(e);
		view.elevUpdate(num, state, floor, e);
	}
	
	//Updates the error status for a specified elevator
	public void errorUpdate(int num, boolean error) {
		elevs[num-1].inError = error;
		now = LocalDateTime.now();
		Event e = new Event(now, "Elevator "+num+" has switched its state of being in error");
		Events.add(e);
		view.errorUpdate(num, error, e);
	}
	
	//Updates recorded destination for a given elevator
	public void destUpdate(int num, int dest) {
		elevs[num-1].dest=dest;
		now = LocalDateTime.now();
		Event e = new Event(now, "Elevator "+num+" has had its destination updated to "+dest);
		Events.add(e);
		view.newDest(num, dest, e);
	}
	
	//Updates the door status of a given elevator
	public void doorUpdate(int num, boolean door) {
		elevs[num-1].door=door;
		now = LocalDateTime.now();
		Event e = new Event(now, "Elevator "+num+" has updated the status of its door");
		Events.add(e);
		view.doorEvent(num, door, e);
	}
	
	//Updates the motor status of a given elevator
	public void motorUpdate(int num, boolean motor) {
		elevs[num-1].motor=motor;
		now = LocalDateTime.now();
		Event e = new Event(now, "Elevator "+num+" has switched the state of its motor");
		Events.add(e);
		view.motorUpdate(num, motor, e);
	}
	
	//Updates the status of a given lamp on a given elevator
	public void lampUpdate(int elnum, int lnum, boolean lamp) {
		elevs[elnum-1].lamps[lnum - 1] = lamp;
		now = LocalDateTime.now();
		Event e = new Event(now, "Elevator "+elnum+" has switched the status of its "+ lnum+" lamp");
		Events.add(e);
		view.lampUpdate(elnum, lnum, lamp, e);
	}
	
	//Updates the status of an Up request on a given floor
	public void upUpdate(int num, boolean up) {
		floors[num-1].up=up;
		now = LocalDateTime.now();
		Event e = new Event(now, "Floor "+num+" has switched the status of its up request button");
		Events.add(e);
		view.floorUpCall(num, up, e);
	}
	
	//Updates the status of a down request on a given floor
	public void downUpdate(int num, boolean down) {
		floors[num-1].down=down;
		now = LocalDateTime.now();
		Event e = new Event(now, "Floor "+num+" has switched the status of its down request button");
		Events.add(e);
		view.floorDownCall(num, down, e);
	}
	
	//Prints out the currently recorded events
	private void printOutFile() {
		// Declare local variables
				Path dir = Paths.get("");
				String absolutePath = dir.toAbsolutePath().toString() + File.separator + "OverseerOutFile.csv";
				
				
				// Write content to file
				try(FileWriter fwriter = new FileWriter(absolutePath)){
					// Write file headers
					if (fwriter == null) {
						System.out.println("is null");
					}
					fwriter.write("Event Time, Event Description\n");
					// Write floor events
					for (int i = 0; i < Events.size(); i++) {
						if (Events.get(i) == null) {
							System.out.println("is null");
						}
						fwriter.write(Events.get(i).now+","+Events.get(i).mess+ "\n");
					}
				} catch (IOException e) {
					e.printStackTrace();
				}
    }

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Overseer o = new Overseer();
		while(true) {
			try {
				TimeUnit.SECONDS.sleep(1);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			o.printOutFile();
		}
		
	}

}
