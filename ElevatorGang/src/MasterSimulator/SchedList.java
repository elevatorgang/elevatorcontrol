package MasterSimulator;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.SocketException;
import java.nio.charset.StandardCharsets;

public class SchedList implements Runnable{
	private SystemView v;
	private DatagramSocket listSock;
	
	public SchedList(SystemView v) {
		this.v = v;
		try {
			listSock = new DatagramSocket(1045);
		} catch (SocketException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	@Override
	public void run() {
		while(true) {
			// TODO Auto-generated method stub
			byte[] data = new byte[100];
			
			DatagramPacket receivePacket = new DatagramPacket(data, data.length);
			
			// Listen on port
			try {
				// Listen on port
				listSock.receive(receivePacket);
			} catch (IOException e) {
				e.printStackTrace();
			}
			
			data = receivePacket.getData();
			String msg = new String(data, StandardCharsets.UTF_8);
			if (msg.indexOf("RMREQ:") != -1) {
				v.removeFReq(msg);
			}else if (msg.indexOf("ADDREQ:")!=-1) {
				v.addFReq(msg);
			}else {
				v.addLog(msg);
			}
		}
	}

}
