package MasterSimulator;

import java.awt.BorderLayout;
import java.awt.Button;
import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;

import java.awt.Dimension;
import java.awt.Label; 
import java.awt.Rectangle;
import java.util.ArrayList;

import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

public class SystemView {//extends JFrame implements Listener{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private static final int LENGTH = 1900;
	private static final int HEIGHT = 1000;
	
	private int numFloors;
	private int numElevators;
	private int numButtons;
	private Overseer overseer;
	//private ArrayList<JButton> floorLightList;
	private JFrame frame;
	private Container pane;
	private DefaultListModel<String> logListModel;
    private JList<String> logList;
    private JScrollPane logScroll;
    private DefaultListModel<String> elevReqsListModel;
    private JList<String> elevReqsList;
    private JScrollPane elevReqsScroll;
    private DefaultListModel<String> elevStateListModel;
    private JList<String> elevStateList;
    private JScrollPane elevStateScroll;
	private JLabel[] Floors;
	private JPanel[] Elevs;
	private JButton[] floorButtons;
	private int LogIndex = 0;
	private ArrayList<ElevState> ElevStates = new ArrayList<ElevState>();
	private SchedList slist;
	private int FLabelH;
	private int FLabelW;
	private class ElevState {

		// Declare class attributes
		public int dest = 0;
		public int floor = 1;
		public String state = "Stationary";
		public String door = "Closed";
		public String motor ="Off";
		public boolean lamps[];
		public String Error = "Operational";
		
		
		// Constructor
		ElevState(int floor){
			lamps = new boolean[floor];
			for (int i = 0 ; i < floor ; i++) {
				lamps[i] = false;
				
			}
		}
	}
	
	
	public SystemView() {
		slist = new SchedList(this);
		frame = new JFrame("Elevator Control Visualization");
		frame.setSize(LENGTH, HEIGHT);
		frame.setResizable(false);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		pane = frame.getContentPane();
		pane.setLayout(null);
		frame.setVisible(true);
	}
	
	public void addLog(String log) {
//		logListModel.addElement(log);
		logListModel.add(LogIndex, log);
		LogIndex++;
		logList.repaint();
	}
	
	public void addFReq(String FReq) {
		int elev = Integer.parseInt(FReq.substring(FReq.indexOf("Elevator:")+9,FReq.indexOf(",Floor:")));
		String t1 = FReq.substring(FReq.indexOf(",Floor:")+7,FReq.indexOf("."));
		int floor = Integer.parseInt(t1);//FReq.substring(FReq.indexOf(",Floor:")+7),FReq.indexOf(".")-1);
		String entry = elevReqsListModel.getElementAt(elev-1);
		if (entry.indexOf(","+Integer.toString(floor)+",")!= -1 || (entry.indexOf(",")!=-1 && entry.substring(entry.lastIndexOf(",")).contentEquals(","+floor))) {
			return;
		}
		entry = entry + "," + floor;
		elevReqsListModel.setElementAt(entry, elev-1);
	}
	
	public void removeFReq(String FReq) {
		int elev = Integer.parseInt(FReq.substring(FReq.indexOf("Elevator:")+9,FReq.indexOf(",Floor:")));
		String floor = FReq.substring(FReq.indexOf(",Floor:")+7,FReq.indexOf("."));
		String entry = elevReqsListModel.getElementAt(elev-1);
		entry = entry.substring(0,entry.indexOf(","+floor))+entry.substring(entry.indexOf(","+floor+",")+floor.length()+2);
		elevReqsListModel.setElementAt(entry, elev-1);
	}
	
	private void populateGUI() {
        int VisualH = frame.getHeight();
        int VisualW = frame.getWidth()*2/3;
        //JPanel window = new JPanel();
        FLabelH = VisualH/(numFloors + 1);
        FLabelW = VisualW/(numElevators+2); // +2 is label space and up/down button space
        int EventLogW = frame.getWidth()/2;
        
        logListModel = new DefaultListModel<String>();
        logList = new JList<String>(logListModel);
        logScroll = new JScrollPane(logList);
        logScroll.setSize(VisualW/2, VisualH/2);
        logScroll.setLocation(VisualW, VisualH/2);
        pane.add(logScroll);
        
        elevReqsListModel = new DefaultListModel<String>();
        elevReqsList = new JList<String>(elevReqsListModel);
        elevReqsScroll = new JScrollPane(elevReqsList);
        elevReqsScroll.setSize(VisualW/4, VisualH/2);
        elevReqsScroll.setLocation(VisualW, 0);
        pane.add(elevReqsScroll);
        
        elevStateListModel = new DefaultListModel<String>();
        elevStateList = new JList<String>(elevStateListModel);
        elevStateScroll = new JScrollPane(elevStateList);
        elevStateScroll.setSize(VisualW/4, VisualH/2);
        elevStateScroll.setLocation(VisualW*5/4, 0);
        pane.add(elevStateScroll);
       
        for (int i = 0; i <= numFloors; i++) {
        	if (i == 0) {
        		JLabel floorTitle = new JLabel();
        		floorTitle.setText("Floor Number");
        		floorTitle.setSize(FLabelW,FLabelH);
        		floorTitle.setLocation(0,(i*FLabelH)-FLabelH/2 + 5);
        		pane.add(floorTitle);
        	}
        	else {
        		Floors[i-1] = new JLabel();
        		Floors[i-1].setLocation(0,(i*FLabelH)-FLabelH/2 );
        		Floors[i-1].setSize(FLabelW,FLabelH);
        		Floors[i-1].setText(Integer.toString(numFloors - i + 1));
        		pane.add(Floors[i-1]);
        		if(i != 1) {
	        		floorButtons[2*i-2] = new JButton((numFloors - i + 1) + "Up");
	        		floorButtons[2*i-2].setLocation(15, (i*FLabelH)-FLabelH/2 - FLabelH/4);
	        		floorButtons[2*i-2].setSize(FLabelW/2, FLabelH/2);
	        		pane.add(floorButtons[2*i-2]);
        		}
        		if(i != numFloors) {
	        		floorButtons[2*i-1] = new JButton((numFloors - i + 1) + "Down");
	        		floorButtons[2*i-1].setSize(FLabelW/2, FLabelH/2);
	        		floorButtons[2*i-1].setLocation(15, (i*FLabelH)-FLabelH/2 + FLabelH/4);
	        		pane.add(floorButtons[2*i-1]);
        		}
        	}
        }
        
        for (int i = 0; i < numElevators; i++) {

        	ElevStates.add(new ElevState(numFloors));
        	elevStateListModel.addElement(eStatetoString(ElevStates.get(i)));
        	Elevs[i] = new JPanel();
        	Elevs[i].setBackground(Color.GREEN);
        	Elevs[i].setSize(new Dimension(FLabelW,FLabelH));
            Elevs[i].setLocation(FLabelW + i*FLabelW+ i*25, numFloors*(FLabelH)-FLabelH/2 + 5); // +2 is label space and up/down button space
            JLabel lab1 = new JLabel("Elevator "+(i+1), JLabel.CENTER);
            lab1.setForeground(Color.BLACK);
            lab1.setVisible(true);
            lab1.setOpaque(true);
            Elevs[i].add(lab1);
            Elevs[i].setOpaque(true);
            Elevs[i].setVisible(true);
            pane.add(Elevs[i]);
            elevReqsListModel.addElement(Integer.toString(i+1)+":");

        }
        frame.repaint();
  
    }
	
	public  void moveElev(int elev, int floor) {
		Elevs[elev-1].setLocation(Elevs[elev-1].getX(), (numFloors-(floor-1))*(FLabelH)-FLabelH/2 + 5);
		frame.repaint();
		
	}
	
	public String eStatetoString(ElevState e) {
		String output = "Current Floor: "+e.floor+ "   Destination: "+e.dest+"   State: "+e.state+"   Door: " + e.door + "   Motor: " + e.motor + "   Lit Lamps: ";
		for (int i = 0 ; i < e.lamps.length ; i++) {
			if(e.lamps[i]) {
				int curr = i+1;
				output=output+" "+curr;
			}
		}
		output = output+"  Elevator Status: "+e.Error;
		
		return output;
		//Silly dance
	}
	
	public  void updateStates() {
	
		elevStateListModel.clear();
		elevStateList.repaint();
		for(int i=0; i<ElevStates.size(); i++) {
			elevStateListModel.addElement(eStatetoString(ElevStates.get(i)));
		}
		elevStateList.repaint();
		
	}
	
	public void initialize(int elevators, int floors) {
		this.numElevators = elevators;
		this.numFloors = floors;
		this.numButtons = floors * 2;
		Floors = new JLabel[floors];
		Elevs = new JPanel[elevators];
		floorButtons = new JButton[floors * 2 + 1];
		populateGUI();
		Thread slThr = new Thread(slist);
		slThr.start();
		
		
	}
	
	public  void logEvent(Event e) {
	
		String log = e.now+" "+e.mess;
		logListModel.add(LogIndex, log);
		LogIndex++;
		logList.repaint();
	
	}
	public void doorEvent(int elevator, boolean isOpen, Event e) {
		//logEvent(e);
		if(isOpen) {
			ElevStates.get(elevator-1).door="Open";
		}else {
			ElevStates.get(elevator-1).door="Closed";
		}
		updateStates();
		
	}
	
	public void elevUpdate(int elevator, int state, int floor, Event e) {
		//logEvent(e);
		ElevStates.get(elevator-1).floor=floor;
		if(state == 0) {
			ElevStates.get(elevator-1).state="Stationary";
		}else if(state == 1){
			ElevStates.get(elevator-1).state="Ascending";
		}else if(state == 2) {
			ElevStates.get(elevator-1).state="Descending";
		}
		updateStates();
		moveElev(elevator, floor);
	}
	
	public void errorUpdate(int elevator, boolean isError, Event e) {
		//logEvent(e);
		if(isError) {
			ElevStates.get(elevator-1).Error="In Error";
			Elevs[elevator-1].setBackground(Color.RED);
		}else {
			ElevStates.get(elevator-1).Error="Operational";
			Elevs[elevator-1].setBackground(Color.GREEN);
		}
		updateStates();
	}
	
	public void newDest(int Elevator, int floor, Event e) {
		//logEvent(e);
		ElevStates.get(Elevator-1).dest=floor;
		updateStates();
	}
	
	public void motorUpdate(int Elevator, boolean isRunning, Event e) {
		//logEvent(e);
		if(isRunning) {
			ElevStates.get(Elevator-1). motor="On";
		}else {
			ElevStates.get(Elevator-1).motor="Off";
		}
		updateStates();
	}
	
	public void lampUpdate(int Elevator, int lamp, boolean isLit, Event e) {
		//logEvent(e);
		ElevStates.get(Elevator-1).lamps[lamp-1]=isLit;
		updateStates();
	}
	
	public void floorUpCall(int floor, boolean isLit, Event e) {
		
		//logEvent(e);
		if (isLit) {
			floorButtons[numButtons - (2 * floor)].setEnabled(false);
		}
		else {
			floorButtons[numButtons - (2 * floor)].setEnabled(true);
		}
		//frame.repaint();
	}
	
	public void floorDownCall(int floor, boolean isLit, Event e) {
		//logEvent(e);
		if (isLit) {
			floorButtons[numButtons - (2 * floor) + 1].setEnabled(false);
		}
		else {
			floorButtons[numButtons - (2 * floor) + 1].setEnabled(true);
		}
		//frame.repaint();
	}
	
	
//	public static void main(String[] args) {
//		new SystemView();
//	}

}
