package MasterSimulator;

import java.time.LocalDateTime;

public class Event{
	public LocalDateTime now;
	public String mess;
	
	Event(LocalDateTime n, String m){
		now = n;
		mess = m;
	}
	
}
