package Logger;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.LinkedBlockingQueue;

public class Logger implements Runnable {
	private int Port;
	private DatagramSocket Socket;
	private LinkedBlockingQueue<String> Logs = new LinkedBlockingQueue<String>();
	
	public Logger(int newPort) {
		Port = newPort;
		try {
			Socket = new DatagramSocket(Port);
		} catch (SocketException e) {
			e.printStackTrace();
		}
	}
	
	public void run() {
		// Print start message
		System.out.println("Logger: Has started running. Listening on port: " + Integer.toString(Port));
		OutFile outF = new OutFile(Logs);
		Thread outThr = new Thread(outF);
		outThr.start();
		while(true) {
			// Listen on port
			Listen();
		}
	}
	
	private void Listen() {
		// Declare receive packet
		byte[] data = new byte[200];
		
		DatagramPacket receivePacket = new DatagramPacket(data, data.length);
		
		// Listen on port
		try {
			// Listen on port
			Socket.receive(receivePacket);
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		data = receivePacket.getData();
		String Log = new String(data, StandardCharsets.UTF_8);//Arrays.toString(data);
		//System.out.println("Logger: Received message: " + Log);
		Logs.add(Log);
		if(Log.indexOf("Scheduler:: Floor Request for Floor ")!=-1&&Log.indexOf("Assigned to elevator ")!=-1) {
			String floor = Log.substring(Log.indexOf("Scheduler:: Floor Request for Floor ")+36,Log.indexOf(" going "));
			String elev = Log.substring(Log.indexOf("Assigned to elevator ")+21,Log.indexOf(".  "));
			String msg = "ADDREQ:,Elevator:"+elev+",Floor:"+floor+".";
			sendLog(msg);
		}else if (Log.indexOf("Sending button press stop confirmation for floor ")!=-1&&Log.indexOf(" for Elevator ")!=-1){
//			String p1 = Log.substring(Log.indexOf("Sending button press stop confirmation for floor ")+49);
//			String p2 = Log.substring(Log.indexOf(" for Elevator ")+14);
//			System.out.println(Log);
//			p2 = Log.substring(Log.indexOf(" to the elevator subsystem"));
			String floor = Log.substring(Log.indexOf("Sending button press stop confirmation for floor ")+49,Log.indexOf(" for Elevator "));
			String elev = Log.substring(Log.indexOf(" for Elevator ")+14,Log.indexOf(" to the elevator subsystem"));
			String msg = "ADDREQ:,Elevator:"+elev+",Floor:"+floor+".";
			sendLog(msg);
		}else if (Log.indexOf(": Sending a new stop at floor ")!=-1 && Log.indexOf(" for elevator ")!=-1) {
			String floor = Log.substring(Log.indexOf(": Sending a new stop at floor ")+30,Log.indexOf(" for elevator "));
			String elev = Log.substring(Log.indexOf(" for elevator ")+14,Log.indexOf(" to the elevator subsystem"));
			String msg = "ADDREQ:,Elevator:"+elev+",Floor:"+floor+".";
			sendLog(msg);
		}else if (Log.indexOf(": Reassigned up floor request on floor ")!=-1&&Log.indexOf(" to elevator ")!=-1) {
			String floor = Log.substring(Log.indexOf(": Reassigned up floor request on floor ")+39,Log.indexOf(" to elevator "));
			String elev = Log.substring(Log.indexOf(" to elevator ")+13);
			String msg = "ADDREQ:,Elevator:"+elev+",Floor:"+floor+".";
			sendLog(msg);
		}else if (Log.indexOf(": Removed stop ")!=-1&&Log.indexOf(" from elevator ")!=-1) {
			String floor = Log.substring(Log.indexOf(": Removed stop ")+15,Log.indexOf(" from elevator "));
			String elev = Log.substring(Log.indexOf(" from elevator ")+15,Log.indexOf(".  "));
			String msg = "RMREQ:,Elevator:"+elev+",Floor:"+floor+".";// + ",YOLO";
//			msg = msg + ",Floor:"+floor+".";
			sendLog(msg);
		}else if (Log.indexOf(": Added stop ")!=-1&&Log.indexOf(" from elevator ")!=-1) {
			String floor = Log.substring(Log.indexOf(": Added stop ")+13,Log.indexOf(" to elevator "));
			String elev = Log.substring(Log.indexOf(" to elevator ")+13);
			String msg = "ADDREQ:,Elevator:"+elev+",Floor:"+floor+".";
			sendLog(msg);
		}else if (Log.indexOf(": Removed stop ")!=-1&&Log.indexOf(" from elevator ")!=-1) {
			String floor = Log.substring(Log.indexOf(": Removed stop ")+15,Log.indexOf(" from elevator "));
			String elev = Log.substring(Log.indexOf(" from elevator ")+15,Log.indexOf(".  "));
			String msg = "RMREQ:,Elevator:"+elev+",Floor:"+floor+".";
			sendLog(msg);
		} else {
			String msg = Log;
			sendLog(msg);
		}
		// Process DatagramPacket
//		ProcessDatagram(receivePacket, data);
	}
	
	private void sendLog(String Msg) {
		byte[] log = Msg.getBytes(StandardCharsets.UTF_8);
		DatagramSocket sendSocket;
		DatagramPacket sendPacket;
		try {
			
			//sendPacket = new DatagramPacket(msg, msg.length, InetAddress.getByName("172.17.40.149"), 1025);
			sendPacket = new DatagramPacket(log, log.length, InetAddress.getByName("172.17.32.255"), 1045);
			sendSocket = new DatagramSocket(1046);
	         sendSocket.send(sendPacket);
	         sendSocket.close();
		} catch (UnknownHostException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	
}
