package Logger;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.LinkedBlockingQueue;

public class OutFile implements Runnable {
	private LinkedBlockingQueue<String> Logs;
	private LinkedBlockingQueue<String> myLogs = new LinkedBlockingQueue<String>();
	
	public OutFile(LinkedBlockingQueue<String> newLogs) {
		Logs = newLogs;
	}
	public void run() {
		// Print start message
		
		while(true) {
			// Listen on port
			String newLog = null;
			try {
				newLog = Logs.take();
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			if (newLog != null) {
				myLogs.add(newLog);
				printOutFile();
			}else {
				try {
					Thread.sleep(1000);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
	}
	
	private void printOutFile() {
		// Declare local variables
		Path dir = Paths.get("");
		String absolutePath = dir.toAbsolutePath().toString() + File.separator + "Logger.csv";
		LinkedBlockingQueue<String> TempL = new LinkedBlockingQueue<String>();// = myLogs;
		
		// Write content to file
		try(FileWriter fwriter = new FileWriter(absolutePath)){
			fwriter.write("Log\n");
			int mlSize = myLogs.size();
			for (int i = 0; i < mlSize; i++) {
				String Temp = myLogs.peek();
				fwriter.write(myLogs.remove()+ "\n");
				TempL.add(Temp);
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		myLogs = TempL;
    }
}
