package TimeSync;

import java.io.IOException;
import java.net.InetAddress;
import java.net.UnknownHostException;

import org.apache.commons.net.ntp.NTPUDPClient;
import org.apache.commons.net.ntp.TimeInfo;

public class TimeSync {
	public static long getTimeOffset() {
		long offset = 0;
		boolean success = false;
		for (int i = 0; i < 5; i++) {
			try {
				NTPUDPClient timeClient = new NTPUDPClient();
				timeClient.setDefaultTimeout(1000);
				InetAddress inetAddress;
				inetAddress = InetAddress.getByName("0.ca.pool.ntp.org");
				long before = System.currentTimeMillis();
				TimeInfo timeInfo = timeClient.getTime(inetAddress);
				long after = System.currentTimeMillis();
				long returnTime = timeInfo.getReturnTime();
				long difference = before - after;
				success = true;
				offset = (after - returnTime - (difference/2));
				System.out.println("TimeSync:: Server Time: " + returnTime + ", This PC Time: " + after + ", Offset set to: " + offset);
				break;
			} catch (UnknownHostException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		if (success == false) {
			System.exit(-1);
		}
		return offset;
	}

}
