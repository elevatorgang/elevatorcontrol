##System Structure
 
-The Elevator System consists of two separate processes; Scheduler Subsystem and Overseer.
 
---
 
##Scheduler Subsystem File Name Explanation

- **UPDATED** SchedulerSimulator.java: The main class of the scheduler, responsible for passing messages between the floor subsystem and the elevator subsystem.
- ElevatorJobQueue.java: The class in charge of sorting new requests as they come in from elevator button presses and floor requests.
- SocketListener.java: Class used to make socket listener threads to relay messages to the main subsystem
- SocketSender.java: Class used to send Datagram Sockets
- ElevatorTimerTask:  Establishes the task to send an error message should a timer not be stopped before reaching its end, signaling a floor sensor miss
---
 
##Elevator Subsystem File Name Explanation

- DatagramMessages.java: The class that is used to send messages from the DatagramSocket Listeners to the Main Processs
- DataSocketListener.java: The class that is used to make the Datagram Socket listener threads
- **UPDATED** Elevator.java:  The class used to simulate the behavior of a single elevator car
- ElevatorButton.java: The class used to simulate a single button in an elevator car.  Pressing it sends a packet to Scheduler to schedule in the destination
- ElevatorDoor.java:  A simple stub to emulate a seperate class actually operating the door of the elevator
- ElevatorLamp.java:  A simple stub to emulate a seperate class operating a floor lamp in the elevator car
- ElevatorMotor.java:  A simple stub simulating operating an actual elevator motor
- **UPDATED** ElevatorSubsystem.java:  The main control process for the subsystem.  It manages incoming packets and executes their instructions
- OutMessage.java: The class that is used to send outgoing messages from the Main Process to the DatagramSocket Listeners
 
---
 
##Floor Subsystem File Name Explanation
 
- **UPDATED** FloorSubsystem.java : The main file for the Floor Subsystem. When this file runs, it first creates the Main Process thread for the Floor Subsystem. The Main Process Thread then creates 2 DataSocketListener Threads which listen for messages from the Scheduler and Elevator Subsystems. Once the Scheduler sends a message stating how many floors to create, the Main Process thread creates the corresponding floor threads
- DataSocketListener.java: The class that is used to make the Datagram Socket listener threads
- **UPDATED** FloorThread.java: The class that is used to make the floor threads
- DatasocketSender.java: The class that is used to send messages from the DatagramSocket Listeners to the Main Processs

---

##Master Simulator File Name Explanation

- **NEW** Overseer.java:  Controller class which records events from the floor and elevator subsystems and outputs them to the GUI
- **NEW** Events.java:  Holding class for event information
- **NEW** SystemView.java:  View class that generates the GUI and provides methods for Overseer.java to update it
- **NEW** SchedList.java:  Receives and processes messages from Scheduler for outputting elevator job queues

---

##Logger File Name Explanation

- **NEW** Logger.java: Receives and records all events in the three main subsystems
- **NEW** OutFile.java: Prints out the events recorded by Logger.java to file.

---
 
**Testing**
 
- ScheduleStub.java: Class used to simulate the Scheduler subsystem for Floor Subsystem testing
- ElevatorStub.java: Class used to simulate the Elevator subsystem for Floor Subsystem testing

---
 
## Floor Subsystem Testing

To test the Floor Subsystem using the Scheduler and Elevator Stubs:

1. Open **FloorSubsystem.java**.
2. Make sure lines 659 to 668 are uncommented
3. Run **FloorSubsystem.java**.

To run the Floor Subsystem with the whole system (without Scheduler and Elevator stubs):

1. Open **FloorSubsystem.java**.
2. Make sure lines 659 to 668 are commented out
3. Run **Overseer.java**.

---
 
## Running the Whole Subsystem
 
To run the whole subsystem:

1. Substitute your computer's IP address for the ones saved as variables in Overseer(lines 25-28), Logger (line 117), and SchedulerSubsystem (lines 69, 70)
2. Run **Overseer.java**.
3. Run **SchedulerSubsystem.java**.
4. Input the desired number of Elevators and Floors, recommended 4 and 22, respectively.

---
 
## Note

The associated diagrams are located within the java project folder.

## Responsibilites of Each Member: Milestone 1

1. Kurt Chavolla Wolf - 100976257: Floor Subsystem
2. Matthew Fundarek - 100941844: Elevator Subsystem and State Diagrams
3. Kaj Hemmingsen-Beriault - 101010131: Scheduler Subsystem

## Responsibilities of Each Member: Milestone 2

1.Kurt - 100976257: Floor Subsystem & Scheduler Subsystem

2.Matthew - 100941844: Elevator Subsystem & Diagrams

3.Kaj - 101010131: Scheduler Subsystem

4.Orinami Adam - 101019916: Scheduler Susbsystem cleanup

## Responsibilities of Each Member: Milestone 3

1.Kurt - 100976257:  Total Scheduler refactor, fault handling and general debug

2.Matthew - 100941844:  Timer and fault implementation, Scheduler and Elevator debug

3.Kaj - 101010131:  Total Scheduler refactor, fault handling and general debug

## Responsibilities of Each Member: Milestone 4

1.Kurt - 100976257: Fixed some bugs in the subsystems, cleaned up subsystem's outputs, added functionality to have subsystems communicate over IP (Multiple computers)

2.Matthew - 100941844: Diagrams, Elevator message output cleanup, Scheduler timers and output file

3.Kaj - 101010131: Added time stamps for console output, started looking into GUI

4.Orinami - 101019916: Worked on the Logger

## Responsibilities of Each Member: Milestone 4

1.Kurt - 100976257: SystemView, Logger and general debugging and refactoring

2.Matthew - 100941844: Diagrams, Overseer, SystemView and general debugging and refactoring

3.Kaj - 101010131: Diagrams, SystemView and general debugging and refactoring

4.Orinami - 101019916: 